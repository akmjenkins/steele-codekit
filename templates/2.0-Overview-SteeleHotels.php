<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero single">
		<div class="swiper-wrapper">
			<div class="swipe" data-controls="true" data-auto="7">
				<div class="swipe-wrap">

					<div data-src="../assets/images/temp/hero/hero-inside-1.jpg">
						<div class="item">&nbsp;</div>
						
						<div class="caption">
							<div class="sw">
								<h1 class="title">Our Locations</h1>
								<span class="sub">Across the Island</span>

							</div><!-- .sw -->
						</div><!-- .caption -->
					</div>
					
				</div><!-- .swipe-wrap -->
			</div><!-- .swipe -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->			

	<div class="body">
		<section>
			<div class="sw">
			
				<div class="grid eqh collapse-no-flex ov-hotel-grid collapse-750">
				
					<div class="col-2 col">
						<a class="ov-hotel item vcenter" href="#" style="background-image: none;">
							<div>
								<img src="../assets/images/hotels/jag-light.svg" alt="JAG Hotel St. John's">
								<div class="content">
									<p>Proin sodales massa et augue congue, ut sodales augue vestibulum. Duis ac ipsum malesuada, pellentesque lectus eget, tristique sem.</p>
									<span class="button dark-bg">More Info</span>
								</div><!-- .content -->
							</div>
						</a><!-- .ov-hotel -->
					</div><!-- .col-2 -->
					
					
					<div class="col-2 col">
						<a class="ov-hotel item vcenter vcenter" href="#" style="background-image: url(../assets/images/temp/hotels/the-capital.jpg);">
							<div>
								<img src="../assets/images/hotels/the-capital-light.svg" alt="The Capital Hotel St. John's">
								
								<div class="content">
									<p>Proin sodales massa et augue congue, ut sodales augue vestibulum. Duis ac ipsum malesuada, pellentesque lectus eget, tristique sem.</p>
									<span class="button dark-bg">More Info</span>
								</div><!-- .content -->
							</div>
						</a><!-- .ov-hotel -->
					</div><!-- .col-2 -->
					
					<div class="col-2 col">
						<a class="ov-hotel item vcenter" href="#" style="background-image: url(../assets/images/temp/hotels/the-albatross.jpg);">
							<div>
								<img src="../assets/images/hotels/the-albatross-light.svg" alt="The Albatross Hotel Gander">
								
								<div class="content">
									<p>Proin sodales massa et augue congue, ut sodales augue vestibulum. Duis ac ipsum malesuada, pellentesque lectus eget, tristique sem.</p>
									<span class="button dark-bg">More Info</span>
								</div><!-- .content -->
							</div>
						</a><!-- .ov-hotel -->
					</div><!-- .col-2 -->
					
					<div class="col-2 col">
						<a class="ov-hotel item vcenter" href="#" style="background-image: url(../assets/images/temp/hotels/sinbads.jpg);">
							<div>
								<img src="../assets/images/hotels/sinbads-light.svg" alt="Sinbad's Hotel & Suites">
								
								<div class="content">
									<p>Proin sodales massa et augue congue, ut sodales augue vestibulum. Duis ac ipsum malesuada, pellentesque lectus eget, tristique sem.</p>
									<span class="button dark-bg">More Info</span>
								</div><!-- .content -->
							</div>
						</a><!-- .ov-hotel -->
					</div><!-- .col-2 -->
					
					<div class="col-2 col">
						<a class="ov-hotel item vcenter" href="#" style="background-image: url(../assets/images/temp/hotels/irving-west.jpg);">
							<div>
								<img src="../assets/images/hotels/irving-west-light.svg" alt="The Irving West Hotel Gander">
								
								<div class="content">
									<p>Proin sodales massa et augue congue, ut sodales augue vestibulum. Duis ac ipsum malesuada, pellentesque lectus eget, tristique sem.</p>
									<span class="button dark-bg">More Info</span>
								</div><!-- .content -->
							</div>
						</a><!-- .ov-hotel -->
					</div><!-- .col-2 -->
					
					<div class="col-2 col">
						<a class="ov-hotel item vcenter" href="#" style="background-image: url(../assets/images/temp/hotels/glynmill.jpg);">
							<div>
								<img src="../assets/images/hotels/glynmill-inn-light.svg" alt="The Glynmill Inn">
								
								<div class="content">
									<p>Proin sodales massa et augue congue, ut sodales augue vestibulum. Duis ac ipsum malesuada, pellentesque lectus eget, tristique sem.</p>
									<span class="button dark-bg">More Info</span>
								</div><!-- .content -->
							</div>
						</a><!-- .ov-hotel -->
					</div><!-- .col-2 -->
					
				</div><!-- .grid -->
			
			</div><!-- .sw -->
		</section>
	</div><!-- .body -->
			

<?php include('inc/i-footer.php'); ?>