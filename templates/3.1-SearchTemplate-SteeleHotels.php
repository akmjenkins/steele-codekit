<?php $bodyclass = 'search'; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero single">
		<div class="swiper-wrapper">
			<div class="swipe" data-controls="true" data-auto="7">
				<div class="swipe-wrap">

					<div data-src="../assets/images/temp/hero/hero-inside-5.jpg">
						<div class="item">&nbsp;</div>
						
						<div class="caption">
							<div class="sw">
								<h1 class="title">Search</h1>
								<span class="sub">Across the Island</span>

							</div><!-- .sw -->
						</div><!-- .caption -->
					</div>
					
				</div><!-- .swipe-wrap -->
			</div><!-- .swipe -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->			

	<div class="body">
	
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="sprite ib home replace">Home</a>
				<a href="#">Search</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->

		<section>
		
			
				<div class="sw">
					
					<div class="hgroup">
						<h2>Top Result</h2>
						<h5 class="light">Quisque feugiat mauris mi ac fringilla</h5>
					</div><!-- .hgroup -->
					
					<div class="featured-block">
						<img src="../assets/images/temp/top-search-result.jpg" alt="Promotion"/>
						
						<div class="content">
							<div class="article-head">
								<span class="tag">Promotion</span>
								Ends September 21, 2014
							</div><!-- .article-head -->
							
							<div class="hgroup">
								<h2>Nullam a ligula eget velit</h2>
								<h5 class="light">Quisque feugiat mauris miac fringilla</h5>
							</div><!-- .hgroup -->
							
							<p>Quisque feugiat mauris mi, ac fringilla erat rutrum non. Morbi consequat massa in massa euismod, ac suscipit sem aliquam. 
							Sed libero felis, feugiat eu hendrerit sit amet, tincidunt gravida purus. Aenean aliquam erat a tincidunt vestibulum. 
							Curabitur placerat lacus at risus ornare convallis. </p>
							
							<a href="#" class="button">Read More</a>

						</div><!-- .content -->
						
					</div><!-- .featured-block -->
					
				</div><!-- .sw -->
			
	
		</section>
	
		<div class="filter-section">
			<div class="filter-bar dark-bg">
				<div class="sw">
					<div class="meta">
					
						<div class="controls">
							<button class="sprite arrow-prev-white">Previous</button>
							<button class="sprite arrow-next-white">Next</button>
						</div><!-- .controls -->
						
					</div><!-- .meta -->
				
					<span class="h6-style title">
						<span class="cat">Pages</span>
						8 Results
					</span><!-- .title -->
				
				</div><!-- .sw -->
			</div><!-- .filter-bar -->
			
			<div class="filter-contents">
				<div class="sw">
				
					<div class="article-body">
						<div class="grid eqh collapse-no-flex blocks collapse-800">
						
							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/search-result-header.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
										
											<h3 class="title">Morbi malesuada nibh non blandit semper</h3>
											<h5 class="light subtitle">Etiam enim lorem, aliquam a iaculis</h5>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/search-result-header.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
										
											<h3 class="title">Morbi malesuada nibh non blandit semper</h3>
											<h5 class="light subtitle">Etiam enim lorem, aliquam a iaculis</h5>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/search-result-header.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
										
											<h3 class="title">Morbi malesuada nibh non blandit semper</h3>
											<h5 class="light subtitle">Etiam enim lorem, aliquam a iaculis</h5>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->
							
						</div><!-- .grid.eqh -->

					</div><!-- .article-body -->
				
				</div><!-- .sw -->
			</div><!-- .filter-contents -->
		</div><!-- .filter-section -->

		<div class="filter-section">
			<div class="filter-bar dark-bg">
				<div class="sw">
					<div class="meta">
					
						<div class="controls">
							<button class="sprite arrow-prev-white">Previous</button>
							<button class="sprite arrow-next-white">Next</button>
						</div><!-- .controls -->
						
					</div><!-- .meta -->
				
					<span class="h6-style title">
						<span class="cat">News</span>
						8 Results
					</span><!-- .title -->
				
				</div><!-- .sw -->
			</div><!-- .filter-bar -->
			
			<div class="filter-contents">
				<div class="sw">
				
					<div class="article-body">
						<div class="grid eqh collapse-no-flex blocks collapse-800">
						
							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/featured-promotion-block-1.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
										
											<div class="article-head">
												<time datetime="2015-10-23">September 23, 2015</time>
											</div>
										
											<h3 class="title">Morbi malesuada nibh non blandit semper</h3>
											<h5 class="light subtitle">Etiam enim lorem, aliquam a iaculis</h5>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/featured-promotion-block-1.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
										
											<div class="article-head">
												<time datetime="2015-10-23">September 23, 2015</time>
											</div>
										
											<h3 class="title">Morbi malesuada nibh non blandit semper</h3>
											<h5 class="light subtitle">Etiam enim lorem, aliquam a iaculis</h5>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->
							
							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/featured-promotion-block-1.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
										
											<div class="article-head">
												<time datetime="2015-10-23">September 23, 2015</time>
											</div>
										
											<h3 class="title">Morbi malesuada nibh non blandit semper</h3>
											<h5 class="light subtitle">Etiam enim lorem, aliquam a iaculis</h5>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->

							
						</div><!-- .grid.eqh -->

					</div><!-- .article-body -->
				
				</div><!-- .sw -->
			</div><!-- .filter-contents -->
		</div><!-- .filter-section -->

		<div class="filter-section">
			<div class="filter-bar dark-bg">
				<div class="sw">
					<div class="meta">
					
						<div class="controls">
							<button class="sprite arrow-prev-white">Previous</button>
							<button class="sprite arrow-next-white">Next</button>
						</div><!-- .controls -->
						
					</div><!-- .meta -->
				
					<span class="h6-style title">
						<span class="cat">Events</span>
						8 Results
					</span><!-- .title -->
				
				</div><!-- .sw -->
			</div><!-- .filter-bar -->
			
			<div class="filter-contents">
				<div class="sw">
				
					<div class="article-body">
						<div class="grid eqh collapse-no-flex blocks collapse-800">
						
							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/featured-promotion-block-1.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
										
											<div class="article-head">
												<time datetime="2015-10-23">September 23, 2015</time>
											</div>
										
											<h3 class="title">Morbi malesuada nibh non blandit semper</h3>
											<h5 class="light subtitle">Etiam enim lorem, aliquam a iaculis</h5>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/featured-promotion-block-1.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
										
											<div class="article-head">
												<time datetime="2015-10-23">September 23, 2015</time>
											</div>
										
											<h3 class="title">Morbi malesuada nibh non blandit semper</h3>
											<h5 class="light subtitle">Etiam enim lorem, aliquam a iaculis</h5>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->
							
							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/featured-promotion-block-1.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
										
											<div class="article-head">
												<time datetime="2015-10-23">September 23, 2015</time>
											</div>
										
											<h3 class="title">Morbi malesuada nibh non blandit semper</h3>
											<h5 class="light subtitle">Etiam enim lorem, aliquam a iaculis</h5>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->

							
						</div><!-- .grid.eqh -->

					</div><!-- .article-body -->
				
				</div><!-- .sw -->
			</div><!-- .filter-contents -->
		</div><!-- .filter-section -->

		<div class="filter-section">
			<div class="filter-bar dark-bg">
				<div class="sw">
					<div class="meta">
					
						<div class="controls">
							<button class="sprite arrow-prev-white">Previous</button>
							<button class="sprite arrow-next-white">Next</button>
						</div><!-- .controls -->
						
					</div><!-- .meta -->
				
					<span class="h6-style title">
						<span class="cat">Promotions</span>
						8 Results
					</span><!-- .title -->
				
				</div><!-- .sw -->
			</div><!-- .filter-bar -->
			
			<div class="filter-contents">
				<div class="sw">
				
					<div class="article-body">
						<div class="grid eqh collapse-no-flex blocks collapse-800">
						
							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/featured-promotion-block-1.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
										
											<div class="article-head">
												<span class="tag">Promotion</span>
												Ends September 21, 2014
											</div><!-- .article-head -->
										
											<h3 class="title">Morbi malesuada nibh non blandit semper</h3>
											<h5 class="light subtitle">Etiam enim lorem, aliquam a iaculis</h5>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->
							
						</div><!-- .grid.eqh -->

					</div><!-- .article-body -->
				
				</div><!-- .sw -->
			</div><!-- .filter-contents -->
		</div><!-- .filter-section -->

		
	</div><!-- .body -->
			

<?php include('inc/i-footer.php'); ?>