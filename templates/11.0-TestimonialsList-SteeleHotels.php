<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero single">
		<div class="swiper-wrapper">
			<div class="swipe" data-controls="true" data-auto="7">
				<div class="swipe-wrap">

					<div data-src="../assets/images/temp/hero/hero-inside-15.jpg">
						<div class="item">&nbsp;</div>
						
						<div class="caption">
							<div class="sw">
								<h1 class="title">Testimonials</h1>
								<span class="sub">Across the Island</span>

							</div><!-- .sw -->
						</div><!-- .caption -->
					</div>
					
				</div><!-- .swipe-wrap -->
			</div><!-- .swipe -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->			

	<div class="body">
	
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="sprite ib home replace">Home</a>
				<a href="#">Media</a>
				<a href="#">Testimonials</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
	
		<div class="filter-section">
			<div class="filter-bar dark-bg">
				<div class="sw">
					<div class="meta">
					
						<form action="/" method="post" class="search-form single-form">
							<fieldset>
								<input type="text" name="s" placeholder="Search testimonials...">
								<button type="submit" class="sprite-after abs search">Search</button>
							</fieldset>
						</form>
						
						<div class="controls">
							<button class="sprite arrow-prev-white">Previous</button>
							<button class="sprite arrow-next-white">Next</button>
						</div><!-- .controls -->
						
					</div><!-- .meta -->
				
					<span class="h6-style title">15 Testimonials</span>
				
				</div><!-- .sw -->
			</div><!-- .filter-bar -->
			
			<div class="filter-contents">
				<div class="sw">
				
					<div class="article-body">
				
						<div class="grid eqh collapse-no-flex fill collapse-750">
						
							<div class="col-2 col">
								<div class="testimonial vcenter">
									<div>
										<h5 class="light">Etiam enim lorem, aliquam a iaculis</h5>
										<p>Sed nec tincidunt ipsum. Nullam a ligula eget velit gravida adipiscing et ut turpis. In hac habitasse platea dictumst. 
										Nam tincidunt tellus sit amet pellentesque semper. Morbi at porttitor magna. Aliquam tincidunt velit ac sem porta, a 
										sagittis ante facilisis. In faucibus purus a enim accumsan laoreet sed vitae tortor.</p>
										
										<em class="light">&mdash; Satisfied Customer</em>
									</div>
								</div><!-- .testimonial -->
							</div><!-- .col -->
							
							<div class="col-2 col">
								<div class="testimonial vcenter">
									<div>
										<h5 class="light">Etiam enim lorem, aliquam a iaculis</h5>
										<p>Sed nec tincidunt ipsum. Nullam a ligula eget velit gravida adipiscing et ut turpis. In hac habitasse platea dictumst. 
										Nam tincidunt tellus sit amet pellentesque semper.</p>
										
										<em class="light">&mdash; Satisfied Customer</em>
									</div>
								</div><!-- .testimonial -->
							</div><!-- .col -->

							<div class="col-2 col">
								<div class="testimonial vcenter">
									<div>
										<h5 class="light">Etiam enim lorem, aliquam a iaculis</h5>
										<p>Sed nec tincidunt ipsum. Nullam a ligula eget velit gravida adipiscing et ut turpis.</p>
										
										<em class="light">&mdash; Satisfied Customer</em>
									</div>
								</div><!-- .testimonial -->
							</div><!-- .col -->
							
							<div class="col-2 col">
								<div class="testimonial vcenter">
									<div>
										<h5 class="light">Etiam enim lorem, aliquam a iaculis</h5>
										<p>Sed nec tincidunt ipsum. Nullam a ligula eget velit gravida adipiscing et ut turpis. In hac habitasse platea dictumst. 
										Nam tincidunt tellus sit amet pellentesque semper.</p>
										
										<em class="light">&mdash; Satisfied Customer</em>
									</div>
								</div><!-- .testimonial -->
							</div><!-- .col -->
							
							<div class="col-2 col">
								<div class="testimonial vcenter">
									<div>
										<h5 class="light">Etiam enim lorem, aliquam a iaculis</h5>
										<p>Sed nec tincidunt ipsum. Nullam a ligula eget velit gravida adipiscing et ut turpis. In hac habitasse platea dictumst. 
										Nam tincidunt tellus sit amet pellentesque semper.</p>
										
										<em class="light">&mdash; Satisfied Customer</em>
									</div>
								</div><!-- .testimonial -->
							</div><!-- .col -->
							
							<div class="col-2 col">
								<div class="testimonial vcenter">
									<div>
										<h5 class="light">Etiam enim lorem, aliquam a iaculis</h5>
										<p>Sed nec tincidunt ipsum. Nullam a ligula eget velit gravida adipiscing et ut turpis. In hac habitasse platea dictumst. 
										Nam tincidunt tellus sit amet pellentesque semper. Morbi at porttitor magna. Aliquam tincidunt velit ac sem porta, a 
										sagittis ante facilisis. In faucibus purus a enim accumsan laoreet sed vitae tortor.</p>
										
										<em class="light">&mdash; Satisfied Customer</em>
									</div>
								</div><!-- .testimonial -->
							</div><!-- .col -->
							
						</div><!-- .grid -->
					
					</div><!-- .article-body -->
				
				</div><!-- .sw -->
			</div><!-- .filter-contents -->
		</div><!-- .filter-section -->
		
	</div><!-- .body -->
			

<?php include('inc/i-footer.php'); ?>