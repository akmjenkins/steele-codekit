<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero single">
		<div class="swiper-wrapper">
			<div class="swipe" data-controls="true" data-auto="7">
				<div class="swipe-wrap">

					<div data-src="../assets/images/temp/hero/hero-inside-8.jpg">
						<div class="item">&nbsp;</div>
						
						<div class="caption">
							<div class="sw">
								<h1 class="title">Morbi Malesuada Nibh non Blandit Semper</h1>
								<span class="sub">Across the Island</span>

							</div><!-- .sw -->
						</div><!-- .caption -->
					</div>
					
				</div><!-- .swipe-wrap -->
			</div><!-- .swipe -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->			

	<div class="body">
	
		<article>
	
			<div class="breadcrumbs">
				<div class="sw">
					<a href="#" class="sprite ib home replace">Home</a>
					<a href="#">News</a>
					<a href="#">Morbi Malesuada Nibh non Blandit Semper</a>
				</div><!-- .sw -->
			</div><!-- .breadcrumbs -->
			
			<section class="sw cf">
				<div class="main-body with-sidebar">
					<div class="article-body">
						
						<div class="article-head">
							<time datetime="2014-09-23">September 23, 2014</time>
						</div><!-- .article-head -->
						
						<p class="excerpt">
							Donec at augue nec ante hendrerit venenatis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec adipiscing ut sem tempus sodales. 
						</p>
						
						<p>Nullam malesuada leo in risus dictum ullamcorper. Fusce elementum, lorem vel varius aliquam, justo massa dignissim tortor, in tempor eros arcu nec ipsum. 
						In tempus mattis libero, sit amet placerat nisl ultrices in. Nulla a fermentum sem. Proin in diam ut enim tristique lobortis. Phasellus porta mollis erat, quis 
						porttitor purus vehicula eu. Vestibulum sit amet lectus magna. Nam et rhoncus turpis. Sed nec feugiat ligula. Donec at erat eros. Quisque eu convallis dui.</p>

						<h5>Donec at Augue nec Ante</h5>
						<p>Duis sagittis luctus risus sed ornare. Maecenas sollicitudin purus non tempor facilisis. Mauris a dictum lectus, ac laoreet est. Nulla facilisi. Duis imperdiet, 
						mi at hendrerit hendrerit, tortor lectus pretium odio, mollis pulvinar nisi nisi accumsan nisi. Donec ut aliquet turpis. Cras nec nisi consequat, lacinia purus eget, 
						ullamcorper orci. Vivamus mauris turpis, iaculis nec imperdiet ac, varius ut nunc.</p>

						<p>Quisque feugiat mauris mi, ac fringilla erat rutrum non. Morbi consequat massa in massa euismod, ac suscipit sem aliquam. Sed libero felis, feugiat eu hendrerit 
						sit amet, tincidunt gravida purus. Aenean aliquam erat a tincidunt vestibulum. Curabitur placerat lacus at risus ornare convallis. Nulla auctor quis lorem ac 
						adipiscing. Maecenas convallis et velit ac posuere. Duis dictum felis sit amet dui sollicitudin, eget posuere sapien viverra. Cras porttitor ornare odio, eu faucibus 
						lorem convallis eu.</p>
						
					</div><!-- .article-body -->
				</div><!-- .main-body -->
				<aside class="sidebar">
					
					<div>
						<form action="/" method="post" class="search-form single-form">
							<fieldset>
								<input type="text" name="s" placeholder="Search news...">
								<button type="submit" class="sprite-after abs search">Search</button>
							</fieldset>
						</form>
					</div>
					
					<div>
					
						<div class="dark-bg head">
							<span class="h5-style">Archives</span>
						</div><!-- .head -->
						
						<div class="accordion">
						
							<div class="accordion-item">
								<div class="accordion-item-handle">
								
									<span class="count">4</span> 2014
								
								</div><!-- .accordion-item-handle -->
								<div class="accordion-item-content">
									
									<ul>
										<li><a href="#">July (3)</a></li>
										<li><a href="#">June (5)</a></li>
										<li><a href="#">May (3)</a></li>
										<li><a href="#">April (1)</a></li>
									</ul>
									
								</div><!-- .accordion-item-content -->
							</div><!-- .accordion-item -->
							
							<div class="accordion-item">
								<div class="accordion-item-handle">
								
									<span class="count">15</span> 2013
								
								</div><!-- .accordion-item-handle -->
								<div class="accordion-item-content">
									
									<ul>
										<li><a href="#">July (3)</a></li>
										<li><a href="#">June (5)</a></li>
										<li><a href="#">May (3)</a></li>
										<li><a href="#">April (1)</a></li>
									</ul>
									
								</div><!-- .accordion-item-content -->
							</div><!-- .accordion-item -->
							
							<div class="accordion-item">
								<div class="accordion-item-handle">
								
									<span class="count">18</span> 2012
								
								</div><!-- .accordion-item-handle -->
								<div class="accordion-item-content">
									
									<ul>
										<li><a href="#">July (3)</a></li>
										<li><a href="#">June (5)</a></li>
										<li><a href="#">May (3)</a></li>
										<li><a href="#">April (1)</a></li>
									</ul>
									
								</div><!-- .accordion-item-content -->
							</div><!-- .accordion-item -->
							
						</div><!-- .accordion -->
					</div>
					
				</aside><!-- .sidebar -->
			</section><!-- .sw -->
		
		</article>
	
	</div><!-- .body -->
			

<?php include('inc/i-footer.php'); ?>