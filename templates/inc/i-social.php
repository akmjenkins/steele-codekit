<div class="social">
	<a href="#" class="sprite facebook-white" title="Like Steele Hotels on Facebook" rel="external">Like Steele Hotels on Facebook</a>
	<a href="#" class="sprite twitter-white" title="Follow Steele Hotels on Twitter" rel="external">Follow Steele Hotels on Twitter</a>
	<a href="#" class="sprite flickr-white" title="Check out Steele Hotels' Photos on Flickr" rel="external">Check out Steele Hotels' Photos on Flickr</a>
	<a href="#" class="sprite instagram-white" title="Check out Steele Hotels' Photos on Instagram" rel="external">Check out Steele Hotels' Photos on Instagram</a>
	<a href="#" class="sprite gplus-white" title="Follow Steele Hotels on Google+" rel="external">Follow Steele Hotels on Google+</a>
	<a href="#" class="sprite youtube-white" title="Watch videos on Steele Hotels' YouTube Channel" rel="external">Watch videos on Steele Hotels' YouTube Channel</a>
</div><!-- .social -->