<?php include dirname(__FILE__) . "/../api-keys.php"; ?>

<div class="tab-wrapper">
	<div class="tab-controls direction-controls">
	
		<div class="selector with-arrow responsive-tab-selector">
			<select class="tab-controller">
				<option selected>By Air</option>
				<option>By Land</option>
				<option>By Water</option>
			</select>
			<span class="value">&nbsp;</span>
		</div><!-- .selector -->
	
		<div class="tbl-wrap">
			<div class="tbl">
			
				<div class="button dark-fill tab-control col-3 center direction-button selected">
					<span class="sprite-before plane">By Air</span>
				</div>
				
				<div class="button dark-fill tab-control col-3 center direction-button">
					<span class="sprite-before car">By Land</span>
				</div>
				
				<div class="button dark-fill tab-control col-3 center direction-button">
					<span class="sprite-before boat">By Water</span>
				</div>
				
			</div>
		</div>
	
	</div><!-- .tab-controls -->
	<div class="tab-holder">
		<div class="tab selected">
			<div class="embedded-gmap">
				<iframe
					frameborder="0" style="border:0"
					src="https://www.google.com/maps/embed/v1/directions?key=<?php echo $GOOGLE_MAPS_API_KEY; ?>
							  &origin=St. John's International Airport
							  &destination=The Glynmill Inn, Corner Brook">
				</iframe>
			</div><!-- .embedded-gmap -->
		</div>
		<div class="tab">
			
			<div class="grid">
				<div class="col-2 col sm-col-1">
				
					<h3>From Address:</h3>
				
					<form action="/" class="single-form" id="street-form">
						<fieldset>
							<input type="text" placeholder="Enter your address...">
							<button type="submit|" class="sprite-before abs search"></button>
						</fieldset>
					</form>
					
				</div>
				<div class="col-2 col sm-col-1 geolocation-only">
					
					<h3>From Current Location:</h3>
					
					<button class="button sprite-after geolocate dark-fill" id="directions-geolocate">Get My Location</button>
					
				</div>
			</div>
			
			
			<div id="street-map" class="embedded-gmap" data-destination="The Glynmill Inn, Corner Brook">
			</div><!-- #street-map -->
			
		</div>
		<div class="tab">
			<div class="embedded-gmap">
				<iframe
					frameborder="0" style="border:0"
					src="https://www.google.com/maps/embed/v1/directions?key=<?php echo $GOOGLE_MAPS_API_KEY; ?>
							  &origin=Marina+Atlantic, Channel Port Aux Basques
							  &destination=The Glynmill Inn, Corner Brook">
				</iframe>
			</div><!-- .embedded-gmap -->
		</div>
	</div><!-- .tab-holder -->
</div><!-- .tab-wrapper -->
