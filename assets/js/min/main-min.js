var ns = 'STEELE';
window[ns] = {};

// @codekit-append "scripts/components/placeholder.js"; 
// @codekit-append "scripts/components/anchors.external.popup.js"; 
// @codekit-append "scripts/components/standard.accordion.js"
// @codekit-append "scripts/components/custom.select.js";
// @codekit-append "scripts/components/magnific.popup.js"; 

// @codekit-append "scripts/components/tabs.js"; 

// Used in date.inputs.js
// @codekit-append "modules/datepicker/datepicker.dev.js" 

// @codekit-append "scripts/components/date.inputs.js"

// Used in swipe.srf.js
// @codekit-append "scripts/components/swipe.js"; 
// @codekit-append "scripts/components/srf.js"; 

// @codekit-append "scripts/swipe.srf.js"; 

// @codekit-append "scripts/device.pixel.ratio.js"; 
// @codekit-append "scripts/search.js"; 
// @codekit-append "scripts/reservation.form.js";
// @codekit-append "scripts/hero.js"; 
// @codekit-append "scripts/nav.js";

// Additional Modernizr Tests
// @codekit-append "modernizr_tests/ios.js";
// @codekit-append "modernizr_tests/android.js";


(function(context) {

		var GOOGLE_MAPS_API_KEY = 'AIzaSyBHIXR1AyV20jkJExujEpPne0lmYoC00Es';
	
		//IE8 doesn't support SVGs
		if(!Modernizr.svg) {
			$('img').each(function() {
				var src = this.src;
				if(src.indexOf('.svg') !== -1) {
					this.src = src.replace(/\.svg$/,'.png');
				}
			});
		}
		
		//contact page
		$('form.contact-form').each(function() {
			var 
				el  =$(this),
				selector = $('select[name=location]',el),
				locations = $('div.loc'),
				updateSelectedIndex = function(i) {
					selector[0].selectedIndex = i+1;
					locations.removeClass('selected').eq(i).addClass('selected');
				};
				
				selector.on('change',function(e) { updateSelectedIndex(this.selectedIndex); });
				locations.on('click',function(e) { e.target.nodeName === 'A' || updateSelectedIndex($(this).parent().index()); });
				
		});
		
		//event map
		$('#view-map')
			.on('click',function(e) {
				$('div.event-map').each(function() {
					var
						el = $(this),
						map = el.children('div.map').data('map')
						
					el.toggleClass('visible');
					google.maps.event.trigger(map.map,'resize');
					map.map.setCenter(map.mapOptions.center);
				});
			});
			
		//blocks
		$(window)
			.on('load resize',function() {
				var blocks = $('.blocks').filter(function() { return !$(this).hasClass('always-list'); });
				
				if(window.outerWidth < 650) {
					blocks.addClass('list-view');
					return;
				}
				
				blocks.each(function(i,blockGroup) {
					var collapseSize = blockGroup.className.match(/collapse-(\d+)/);
					if(collapseSize.length == 2 && window.outerWidth < (+collapseSize[1])) {
						$(blockGroup).addClass('list-view');
					} else {
						$(blockGroup).removeClass('list-view');
					}
				});
				
			}).trigger('resize');
		
		//extras list
		$('div.hotel-nav-extras-list').each(function() {
			var el = $(this);
			el.on('click','span.toggle',function(e) {
				el.toggleClass('expanded');
			});
		});
		
		//responsive hotel nav
		$('div.hotel-nav select').on('change',function(e) {
			if(this.value.length && window.location.href != this.value) {
				window.location.href = this.value;
			}
		});
		
		//directions geolocate
		$('#street-map').each(function() {
			var
				el = $(this),
				geolocate = $('#directions-geolocate'),
				search = $('#street-form'),
				buildIFrame = function(origin) {
					var source = "https://www.google.com/maps/embed/v1/directions?mode=driving";
					source+='&key='+GOOGLE_MAPS_API_KEY;
					source+='&destination='+el.data('destination');				
					source+='&origin='+origin;
					el.html('<iframe src="'+source+'" frameborder="0" style="border:0"/>');
				},
				onGeolocateDone = function(position) {
					buildIFrame(position.coords.latitude+','+position.coords.longitude);
				},
				onGeolocateFail = function(positionError) {
					alert(positionError.message);
					el.removeClass('loading');				
				};
				
			search
				.on('submit',function(e) {
					buildIFrame($(this).find('input').val());
					return false;
				});
			
			if(navigator.geolocation) {
			
				geolocate.on('click',function(e) {
					el.addClass('loading');
					navigator.geolocation.getCurrentPosition(onGeolocateDone,onGeolocateFail);
				});
			
			} else {
				geolocate.closest('div.col').addClass('i-hidden');
			}
				
		});
		
		//activities search
		$('#activities-map').each(function() {
			var
				el = $(this),
				form = $('#activities-form');
				
				form.on('submit',function(e) {
					var input = form.find('input');
					var source = "https://www.google.com/maps/embed/v1/search?";
					source+='&key='+GOOGLE_MAPS_API_KEY;
					source+='&center='+el.data('center');
					source+='&zoom='+el.data('zoom');
					source+='&q='+input.val();
					el.html('<iframe src="'+source+'" frameborder="0" style="border:0"/>');
					return false;
				});
		});

}(window[ns]));

(function(context) {
	/* jquery placeholder plugin */
	if(!Modernizr.placeholder) {
		(function($){$.Placeholder={settings:{color:"rgb(169,169,169)",dataName:"original-font-color"},init:function(c){if(c){$.extend($.Placeholder.settings,c)}var d=function(a){return $(a).val()};var e=function(a,b){$(a).val(b)};var f=function(a){return $(a).attr("placeholder")};var g=function(a){var b=d(a);return(b.length===0)||b==f(a)};var h=function(a){$(a).data($.Placeholder.settings.dataName,$(a).css("color"));$(a).css("color",$.Placeholder.settings.color)};var i=function(a){$(a).css("color",$(a).data($.Placeholder.settings.dataName));$(a).removeData($.Placeholder.settings.dataName)};var j=function(a){e(a,f(a));h(a)};var k=function(a){if($(a).data($.Placeholder.settings.dataName)){e(a,"");i(a)}};var l=function(){if(g(this)){k(this)}};var m=function(){if(g(this)){j(this)}};var n=function(){if(g(this)){k(this)}};$("textarea, input[type='text'],input[type='email'],input[type='tel'],input[type='search']").each(function(a,b){if($(b).attr("placeholder")){$(b).focus(l);$(b).blur(m);$(b).bind("parentformsubmitted",n);$(b).trigger("blur");$(b).parents("form").submit(function(){$(b).trigger("parentformsubmitted")})}});return this}}})(jQuery);
		$(document).ready(function() { $.Placeholder.init(); });
	}
}(window[ns]));

//anchor listeners
(function(context) {

	//pub/sub listeners on the document
	$(document)
	
		//link events
		.on('click','a[rel=external]',function (e) { 
			e.preventDefault();
			window.open(this.href); 
		})
		.on('click','a[rel=popup]',function () { 
			var 	el 				= $(this),
					name			=	el.data('name') || (new Date()).getTime(),
					opts				=	el.data('opts'),
					optsString		=	'';
					
			//make all popups that don't explicity say so have scrollbars and are resizeable
			if(!opts) {
				opts = {scrollbars : 'yes',resizeable:'yes'};
			}
			
			if(typeof opts !== 'object') { opts = $.parseJSON(opts); }
		
			opts.resizable = (opts.resizable) ? opts.resizable : 'yes';
			opts.scrollbars = (opts.scrollbars) ? opts.scrollbars : 'yes';
		
			for(i in opts) {
				//don't allow a window to open that is taller than the browser
				if(i === 'height') {
					opts[i] = (window.screen.height < (+opts[i])) ? window.screen.height-50 : opts[i];
				} else if(i === 'width') {
					opts[i] = (window.screen.width < (+opts[i])) ? window.screen.width-50 : opts[i];
				}
				
				optsString	+= i+'='+opts[i]+',';
			}

			window.open(this.href, name,optsString);
			return false; 
		});
		
}(window[ns]));

//standard accordion
(function(context) {
	var opts;
	$('div.accordion')
		.on('click','div.accordion-item-handle',function(e) {
			var 
				el = $(this).parent(),
				acc = el.closest('div.accordion');
				
			e.preventDefault();
			
			if(acc.hasClass('inactive') || el.hasClass('inactive')) { return; }
			
			opts = {duration:450};
			
			if(el.hasClass('expanded')) {
				el.removeClass('expanded').children('div.accordion-item-content').stop().slideUp(opts);
			} else {
				el.addClass('expanded').children('div.accordion-item-content').stop().slideDown(opts);
			}
			
			acc.hasClass('allow-multiple') || el.siblings().removeClass('expanded').children('div.accordion-item-content').stop().slideUp(opts);
			
			//no bubble;
			return false;
			
		}).each(function() {
			$(this).find('div.expanded').children('div.accordion-item-content').slideDown(opts);
		});
	
	
	
}(window[ns]));

(function(context) {

		//selector wrapper
		$('div.selector').each(function() {
			var 
				el = $(this),
				select = $('select',el),
				val = $('span.value',el);
				
			select
				.on('change',function(e) { val.html($(this).children('option:selected').text()); })
				.trigger('change');
		});
		
}(window[ns]));

(function($,context) {

		var getMagnificItemForEl = function(el) {
			var 
				magnificItem = {},
				type = el.data('type');
				magnificItem.src = el[0].href || el.data('src');
				magnificItem.titleSrc = el.data('title');
				
				//a little help
				if(magnificItem.src.match(/vimeo|youtube|maps/i)) {
					type = 'iframe';
				} else if(magnificItem.src.match(/(jpg|gif|png|jpeg)(\?.*)?$/i)) {
					type = 'image';
				}
				
				
			switch(type) {
				case 'image':
					magnificItem.type = 'image';
					break;
				case 'inline':
					magnificItem.type = 'inline';
					magnificItem.src = $(magnificItem.src).html();
					break;
				case 'ajax':
					magnificItem.type = 'ajax';
					break;
				case 'iframe':
				case 'video':
				case 'map':
					magnificItem.type = 'iframe';
					break;
			}
			
			return magnificItem;
		};

		//magnific popup
		$(document)
			.on('click','.mpopup',function(e) {
				e.preventDefault();
				var 
					magnificItems,
					items,
					isPartOfGallery,
					thisIndex = 0,
					el = $(this),
					galleryName = el.data('gallery');
					
					isPartOfGallery = !!galleryName;
				
				if(isPartOfGallery) {
					magnificItems = [];
					items = $('.mpopup').filter(function(i) { return $(this).data('gallery') === galleryName; });
					
					items.each(function(i) { 
						if(this === el[0]) { 
							thisIndex = i;  
						} 
						magnificItems.push(getMagnificItemForEl($(this)));
						return true; 
					});
				} else {
					magnificItems = getMagnificItemForEl(el);
				}
				
				$.magnificPopup.open({
					items:magnificItems,
					disableOn: 700,
					mainClass: 'mfp-fade',
					removalDelay: 160,
					preloader: true,
					fixedContentPos: false,
					gallery:{enabled:isPartOfGallery}
				},thisIndex);
			
			});
			
			
		//no public API
		return {};

}(jQuery,window[ns]));

(function(context) {


		$(document)
			.on('change','select.tab-controller',function(e) {

				var
					el = $(this),
					wrapper = el.closest('div.tab-wrapper'),
					tabControlWrapper = wrapper.children('div.tab-controls'),
					controls = tabControlWrapper.find('.tab-control'),
					holder = wrapper.children('div.tab-holder'),
					tabs = holder.children('div.tab'),
					tabToShow = tabs.eq(this.selectedIndex);

				if(controls) {
					controls.removeClass('selected').eq(tabToShow.index()).addClass('selected');
				}

				tabs.removeClass('selected');
				tabToShow.addClass('selected');

			})
			.on('click','.tab-control',function(e) {
				e.preventDefault();

				var
					el = $(this),
					wrapper = el.closest('div.tab-wrapper'),
					tabControlWrapper = wrapper.children('div.tab-controls'),
					controls = tabControlWrapper.find('.tab-control'),
					selectorControl = tabControlWrapper.find('select.tab-controller'),
					holder = wrapper.children('div.tab-holder'),
					tabs = holder.children('div.tab'),
					tabToShow;
			
				
				if(el.data('selector')) {
					tabToShow = $(el.data('selector'));
				}
				
				tabToShow = (tabToShow && tabToShow.length) ? tabToShow : tabs.eq(el.index());

				if(selectorControl.length) {
					selectorControl[0].selectedIndex = tabToShow.index();
					selectorControl.trigger('change');
				} else {
					controls.removeClass('selected').eq(tabToShow.index()).addClass('selected');
					tabs.removeClass('selected');
					tabToShow.addClass('selected');					
				}
				
			});
	
}(window[ns]));

(function(jQuery,context) {

	/**
	 *
	 * Date picker
	 * Author: Stefan Petre www.eyecon.ro
	 * 
	 * Dual licensed under the MIT and GPL licenses
	 * 
	 */
	(function ($) {
		var DatePicker = function () {
			var	ids = {},
				views = {
					years: 'datepickerViewYears',
					months: 'datepickerViewMonths',
					days: 'datepickerViewDays'
				},
				tpl = {
					wrapper: '<div class="datepicker"><div class="datepickerContainer"><table cellspacing="0" cellpadding="0"><tbody><tr></tr></tbody></table></div></div>',
					head: [
						'<td>',
						'<table cellspacing="0" cellpadding="0">',
							'<thead>',
								'<tr>',
									'<th class="datepickerGoPrev"><a href="#"><span><%=prev%></span></a></th>',
									'<th colspan="5" class="datepickerMonth"><a href="#"><span></span></a></th>',
									'<th class="datepickerGoNext"><a href="#"><span><%=next%></span></a></th>',
								'</tr>',
								'<tr class="datepickerDoW">',
									'<th class="no"><span>&nbsp;</span></th>',
									'<th><span><%=day1%></span></th>',
									'<th><span><%=day2%></span></th>',
									'<th><span><%=day3%></span></th>',
									'<th><span><%=day4%></span></th>',
									'<th><span><%=day5%></span></th>',
									'<th><span><%=day6%></span></th>',
									'<th><span><%=day7%></span></th>',
								'</tr>',
							'</thead>',
						'</table></td>'
					],
					space : '<td class="datepickerSpace"><div></div></td>',
					days: [
						'<tbody class="datepickerDays">',
							'<tr>',
								'<td class="no"><span>&nbsp;</span></td>',
								'<td class="<%=weeks[0].days[0].classname%>"><a href="#"><span><%=weeks[0].days[0].text%></span></a></td>',
								'<td class="<%=weeks[0].days[1].classname%>"><a href="#"><span><%=weeks[0].days[1].text%></span></a></td>',
								'<td class="<%=weeks[0].days[2].classname%>"><a href="#"><span><%=weeks[0].days[2].text%></span></a></td>',
								'<td class="<%=weeks[0].days[3].classname%>"><a href="#"><span><%=weeks[0].days[3].text%></span></a></td>',
								'<td class="<%=weeks[0].days[4].classname%>"><a href="#"><span><%=weeks[0].days[4].text%></span></a></td>',
								'<td class="<%=weeks[0].days[5].classname%>"><a href="#"><span><%=weeks[0].days[5].text%></span></a></td>',
								'<td class="<%=weeks[0].days[6].classname%>"><a href="#"><span><%=weeks[0].days[6].text%></span></a></td>',
							'</tr>',
							'<tr>',
								'<td class="no"><span>&nbsp;</span></td>',
								'<td class="<%=weeks[1].days[0].classname%>"><a href="#"><span><%=weeks[1].days[0].text%></span></a></td>',
								'<td class="<%=weeks[1].days[1].classname%>"><a href="#"><span><%=weeks[1].days[1].text%></span></a></td>',
								'<td class="<%=weeks[1].days[2].classname%>"><a href="#"><span><%=weeks[1].days[2].text%></span></a></td>',
								'<td class="<%=weeks[1].days[3].classname%>"><a href="#"><span><%=weeks[1].days[3].text%></span></a></td>',
								'<td class="<%=weeks[1].days[4].classname%>"><a href="#"><span><%=weeks[1].days[4].text%></span></a></td>',
								'<td class="<%=weeks[1].days[5].classname%>"><a href="#"><span><%=weeks[1].days[5].text%></span></a></td>',
								'<td class="<%=weeks[1].days[6].classname%>"><a href="#"><span><%=weeks[1].days[6].text%></span></a></td>',
							'</tr>',
							'<tr>',
								'<td class="no"><span>&nbsp;</span></td>',
								'<td class="<%=weeks[2].days[0].classname%>"><a href="#"><span><%=weeks[2].days[0].text%></span></a></td>',
								'<td class="<%=weeks[2].days[1].classname%>"><a href="#"><span><%=weeks[2].days[1].text%></span></a></td>',
								'<td class="<%=weeks[2].days[2].classname%>"><a href="#"><span><%=weeks[2].days[2].text%></span></a></td>',
								'<td class="<%=weeks[2].days[3].classname%>"><a href="#"><span><%=weeks[2].days[3].text%></span></a></td>',
								'<td class="<%=weeks[2].days[4].classname%>"><a href="#"><span><%=weeks[2].days[4].text%></span></a></td>',
								'<td class="<%=weeks[2].days[5].classname%>"><a href="#"><span><%=weeks[2].days[5].text%></span></a></td>',
								'<td class="<%=weeks[2].days[6].classname%>"><a href="#"><span><%=weeks[2].days[6].text%></span></a></td>',
							'</tr>',
							'<tr>',
								'<td class="no"><span>&nbsp;</span></td>',
								'<td class="<%=weeks[3].days[0].classname%>"><a href="#"><span><%=weeks[3].days[0].text%></span></a></td>',
								'<td class="<%=weeks[3].days[1].classname%>"><a href="#"><span><%=weeks[3].days[1].text%></span></a></td>',
								'<td class="<%=weeks[3].days[2].classname%>"><a href="#"><span><%=weeks[3].days[2].text%></span></a></td>',
								'<td class="<%=weeks[3].days[3].classname%>"><a href="#"><span><%=weeks[3].days[3].text%></span></a></td>',
								'<td class="<%=weeks[3].days[4].classname%>"><a href="#"><span><%=weeks[3].days[4].text%></span></a></td>',
								'<td class="<%=weeks[3].days[5].classname%>"><a href="#"><span><%=weeks[3].days[5].text%></span></a></td>',
								'<td class="<%=weeks[3].days[6].classname%>"><a href="#"><span><%=weeks[3].days[6].text%></span></a></td>',
							'</tr>',
							'<tr>',
								'<td class="no"><span>&nbsp;</span></td>',
								'<td class="<%=weeks[4].days[0].classname%>"><a href="#"><span><%=weeks[4].days[0].text%></span></a></td>',
								'<td class="<%=weeks[4].days[1].classname%>"><a href="#"><span><%=weeks[4].days[1].text%></span></a></td>',
								'<td class="<%=weeks[4].days[2].classname%>"><a href="#"><span><%=weeks[4].days[2].text%></span></a></td>',
								'<td class="<%=weeks[4].days[3].classname%>"><a href="#"><span><%=weeks[4].days[3].text%></span></a></td>',
								'<td class="<%=weeks[4].days[4].classname%>"><a href="#"><span><%=weeks[4].days[4].text%></span></a></td>',
								'<td class="<%=weeks[4].days[5].classname%>"><a href="#"><span><%=weeks[4].days[5].text%></span></a></td>',
								'<td class="<%=weeks[4].days[6].classname%>"><a href="#"><span><%=weeks[4].days[6].text%></span></a></td>',
							'</tr>',
							'<tr>',
								'<td class="no"><span>&nbsp;</span></td>',
								'<td class="<%=weeks[5].days[0].classname%>"><a href="#"><span><%=weeks[5].days[0].text%></span></a></td>',
								'<td class="<%=weeks[5].days[1].classname%>"><a href="#"><span><%=weeks[5].days[1].text%></span></a></td>',
								'<td class="<%=weeks[5].days[2].classname%>"><a href="#"><span><%=weeks[5].days[2].text%></span></a></td>',
								'<td class="<%=weeks[5].days[3].classname%>"><a href="#"><span><%=weeks[5].days[3].text%></span></a></td>',
								'<td class="<%=weeks[5].days[4].classname%>"><a href="#"><span><%=weeks[5].days[4].text%></span></a></td>',
								'<td class="<%=weeks[5].days[5].classname%>"><a href="#"><span><%=weeks[5].days[5].text%></span></a></td>',
								'<td class="<%=weeks[5].days[6].classname%>"><a href="#"><span><%=weeks[5].days[6].text%></span></a></td>',
							'</tr>',
						'</tbody>'
					],
					months: [
						'<tbody class="<%=className%>">',
							'<tr>',
								'<td colspan="2"><a href="#"><span><%=data[0]%></span></a></td>',
								'<td colspan="2"><a href="#"><span><%=data[1]%></span></a></td>',
								'<td colspan="2"><a href="#"><span><%=data[2]%></span></a></td>',
								'<td colspan="2"><a href="#"><span><%=data[3]%></span></a></td>',
							'</tr>',
							'<tr>',
								'<td colspan="2"><a href="#"><span><%=data[4]%></span></a></td>',
								'<td colspan="2"><a href="#"><span><%=data[5]%></span></a></td>',
								'<td colspan="2"><a href="#"><span><%=data[6]%></span></a></td>',
								'<td colspan="2"><a href="#"><span><%=data[7]%></span></a></td>',
							'</tr>',
							'<tr>',
								'<td colspan="2"><a href="#"><span><%=data[8]%></span></a></td>',
								'<td colspan="2"><a href="#"><span><%=data[9]%></span></a></td>',
								'<td colspan="2"><a href="#"><span><%=data[10]%></span></a></td>',
								'<td colspan="2"><a href="#"><span><%=data[11]%></span></a></td>',
							'</tr>',
						'</tbody>'
					]
				},
				defaults = {
					flat: false,
					starts: 1,
					prev: '&#9664;',
					next: '&#9654;',
					lastSel: false,
					mode: 'single',
					view: 'days',
					calendars: 1,
					format: 'Y-m-d',
					position: 'bottom',
					eventName: 'click',
					onRender: function(){return {};},
					onChange: function(){return true;},
					onShow: function(){return true;},
					onBeforeShow: function(){return true;},
					onHide: function(){return true;},
					locale: {
						days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
						daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
						daysMin: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
						//daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"],
						months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
						monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
						weekMin: 'wk'
					}
				},
				fill = function(el) {
					var options = $(el).data('datepicker');
					var cal = $(el);
					var currentCal = Math.floor(options.calendars/2), date, data, dow, month, cnt = 0, week, days, indic, indic2, html, tblCal;
					cal.find('td>table tbody').remove();
					for (var i = 0; i < options.calendars; i++) {
						date = new Date(options.current);
						date.addMonths(-currentCal+i);
						tblCal = cal.find('table').eq(i+1);
						switch (tblCal[0].className) {
							case 'datepickerViewDays':
								dow = formatDate(date, 'B, Y');
								break;
							case 'datepickerViewMonths':
								dow = date.getFullYear();
								break;
							case 'datepickerViewYears':
								dow = (date.getFullYear()-6) + ' - ' + (date.getFullYear()+5);
								break;
						} 
						tblCal.find('thead tr:first th:eq(1) span').text(dow);
						dow = date.getFullYear()-6;
						data = {
							data: [],
							className: 'datepickerYears'
						}
						for ( var j = 0; j < 12; j++) {
							data.data.push(dow + j);
						}
						html = tmpl(tpl.months.join(''), data);
						date.setDate(1);
						data = {weeks:[], test: 10};
						month = date.getMonth();
						var dow = (date.getDay() - options.starts) % 7;
						date.addDays(-(dow + (dow < 0 ? 7 : 0)));
						week = -1;
						cnt = 0;
						while (cnt < 42) {
							indic = parseInt(cnt/7,10);
							indic2 = cnt%7;
							if (!data.weeks[indic]) {
								week = date.getWeekNumber();
								data.weeks[indic] = {
									week: week,
									days: []
								};
							}
							data.weeks[indic].days[indic2] = {
								text: date.getDate(),
								classname: []
							};
							if (month != date.getMonth()) {
							
								if (date.getMonth() > month) {
									if(date.getMonth() === 11 && month === 0) {} else {
										data.weeks[indic].days[indic2].classname.push('datepickerAfterMonth');	
									}
								}
							
								data.weeks[indic].days[indic2].classname.push('datepickerNotInMonth');
							}
							if (date.getDay() == 0) {
								data.weeks[indic].days[indic2].classname.push('datepickerSunday');
							}
							if (date.getDay() == 6) {
								data.weeks[indic].days[indic2].classname.push('datepickerSaturday');
							}
							var fromUser = options.onRender(date);
							var val = date.valueOf();
							if (fromUser.selected || options.date == val || $.inArray(val, options.date) > -1 || (options.mode == 'range' && val >= options.date[0] && val <= options.date[1])) {
								data.weeks[indic].days[indic2].classname.push('datepickerSelected');
							}
							if (fromUser.disabled) {
								data.weeks[indic].days[indic2].classname.push('datepickerDisabled');
							}
							if (fromUser.className) {
								data.weeks[indic].days[indic2].classname.push(fromUser.className);
							}
							data.weeks[indic].days[indic2].classname = data.weeks[indic].days[indic2].classname.join(' ');
							cnt++;
							date.addDays(1);
						}
						html = tmpl(tpl.days.join(''), data) + html;
						data = {
							data: options.locale.monthsShort,
							className: 'datepickerMonths'
						};
						html = tmpl(tpl.months.join(''), data) + html;
						tblCal.append(html);
					}
				},
				parseDate = function (date, format) {
					if (date.constructor == Date) {
						return new Date(date);
					}
					var parts = date.split(/\W+/);
					var against = format.split(/\W+/), d, m, y, h, min, now = new Date();
					for (var i = 0; i < parts.length; i++) {
						switch (against[i]) {
							case 'd':
							case 'e':
								d = parseInt(parts[i],10);
								break;
							case 'm':
								m = parseInt(parts[i], 10)-1;
								break;
							case 'Y':
							case 'y':
								y = parseInt(parts[i], 10);
								y += y > 100 ? 0 : (y < 29 ? 2000 : 1900);
								break;
							case 'H':
							case 'I':
							case 'k':
							case 'l':
								h = parseInt(parts[i], 10);
								break;
							case 'P':
							case 'p':
								if (/pm/i.test(parts[i]) && h < 12) {
									h += 12;
								} else if (/am/i.test(parts[i]) && h >= 12) {
									h -= 12;
								}
								break;
							case 'M':
								min = parseInt(parts[i], 10);
								break;
						}
					}
					return new Date(
						y === undefined ? now.getFullYear() : y,
						m === undefined ? now.getMonth() : m,
						d === undefined ? now.getDate() : d,
						h === undefined ? now.getHours() : h,
						min === undefined ? now.getMinutes() : min,
						0
					);
				},
				formatDate = function(date, format) {
					var m = date.getMonth();
					var d = date.getDate();
					var y = date.getFullYear();
					var wn = date.getWeekNumber();
					var w = date.getDay();
					var s = {};
					var hr = date.getHours();
					var pm = (hr >= 12);
					var ir = (pm) ? (hr - 12) : hr;
					var dy = date.getDayOfYear();
					if (ir == 0) {
						ir = 12;
					}
					var min = date.getMinutes();
					var sec = date.getSeconds();
					var parts = format.split(''), part;
					for ( var i = 0; i < parts.length; i++ ) {
						part = parts[i];
						switch (parts[i]) {
							case 'a':
								part = date.getDayName();
								break;
							case 'A':
								part = date.getDayName(true);
								break;
							case 'b':
								part = date.getMonthName();
								break;
							case 'B':
								part = date.getMonthName(true);
								break;
							case 'C':
								part = 1 + Math.floor(y / 100);
								break;
							case 'd':
								part = (d < 10) ? ("0" + d) : d;
								break;
							case 'e':
								part = d;
								break;
							case 'H':
								part = (hr < 10) ? ("0" + hr) : hr;
								break;
							case 'I':
								part = (ir < 10) ? ("0" + ir) : ir;
								break;
							case 'j':
								part = (dy < 100) ? ((dy < 10) ? ("00" + dy) : ("0" + dy)) : dy;
								break;
							case 'k':
								part = hr;
								break;
							case 'l':
								part = ir;
								break;
							case 'm':
								part = (m < 9) ? ("0" + (1+m)) : (1+m);
								break;
							case 'M':
								part = (min < 10) ? ("0" + min) : min;
								break;
							case 'p':
							case 'P':
								part = pm ? "PM" : "AM";
								break;
							case 's':
								part = Math.floor(date.getTime() / 1000);
								break;
							case 'S':
								part = (sec < 10) ? ("0" + sec) : sec;
								break;
							case 'u':
								part = w + 1;
								break;
							case 'w':
								part = w;
								break;
							case 'y':
								part = ('' + y).substr(2, 2);
								break;
							case 'Y':
								part = y;
								break;
						}
						parts[i] = part;
					}
					return parts.join('');
				},
				extendDate = function(options) {
					if (Date.prototype.tempDate) {
						return;
					}
					Date.prototype.tempDate = null;
					Date.prototype.months = options.months;
					Date.prototype.monthsShort = options.monthsShort;
					Date.prototype.days = options.days;
					Date.prototype.daysShort = options.daysShort;
					Date.prototype.getMonthName = function(fullName) {
						return this[fullName ? 'months' : 'monthsShort'][this.getMonth()];
					};
					Date.prototype.getDayName = function(fullName) {
						return this[fullName ? 'days' : 'daysShort'][this.getDay()];
					};
					Date.prototype.addDays = function (n) {
						this.setDate(this.getDate() + n);
						this.tempDate = this.getDate();
					};
					Date.prototype.addMonths = function (n) {
						if (this.tempDate == null) {
							this.tempDate = this.getDate();
						}
						this.setDate(1);
						this.setMonth(this.getMonth() + n);
						this.setDate(Math.min(this.tempDate, this.getMaxDays()));
					};
					Date.prototype.addYears = function (n) {
						if (this.tempDate == null) {
							this.tempDate = this.getDate();
						}
						this.setDate(1);
						this.setFullYear(this.getFullYear() + n);
						this.setDate(Math.min(this.tempDate, this.getMaxDays()));
					};
					Date.prototype.getMaxDays = function() {
						var tmpDate = new Date(Date.parse(this)),
							d = 28, m;
						m = tmpDate.getMonth();
						d = 28;
						while (tmpDate.getMonth() == m) {
							d ++;
							tmpDate.setDate(d);
						}
						return d - 1;
					};
					Date.prototype.getFirstDay = function() {
						var tmpDate = new Date(Date.parse(this));
						tmpDate.setDate(1);
						return tmpDate.getDay();
					};
					Date.prototype.getWeekNumber = function() {
						var tempDate = new Date(this);
						tempDate.setDate(tempDate.getDate() - (tempDate.getDay() + 6) % 7 + 3);
						var dms = tempDate.valueOf();
						tempDate.setMonth(0);
						tempDate.setDate(4);
						return Math.round((dms - tempDate.valueOf()) / (604800000)) + 1;
					};
					Date.prototype.getDayOfYear = function() {
						var now = new Date(this.getFullYear(), this.getMonth(), this.getDate(), 0, 0, 0);
						var then = new Date(this.getFullYear(), 0, 0, 0, 0, 0);
						var time = now - then;
						return Math.floor(time / 24*60*60*1000);
					};
				},
				layout = function (el) {
					var options = $(el).data('datepicker');
					var cal = $('#' + options.id);
					if (!options.extraHeight) {
						var divs = $(el).find('div');

						options.extraHeight = divs.get(0).offsetHeight + divs.get(1).offsetHeight;
						options.extraWidth = divs.get(2).offsetWidth + divs.get(3).offsetWidth;
					}
					var tbl = cal.find('table:first').get(0);
					var width = tbl.offsetWidth;
					var height = tbl.offsetHeight;
					cal.css({
						width: width + options.extraWidth + 'px',
						height: height + options.extraHeight + 'px'
					}).find('div.datepickerContainer').css({
						width: width + 'px',
						height: height + 'px'
					});
				},
				click = function(ev) {
					if ($(ev.target).is('span')) {
						ev.target = ev.target.parentNode;
					}
					var el = $(ev.target);
					if (el.is('a')) {
						ev.target.blur();
						if (el.hasClass('datepickerDisabled')) {
							return false;
						}
						var options = $(this).data('datepicker');
						var parentEl = el.parent();
						var tblEl = parentEl.parent().parent().parent();
						var tblIndex = $('table', this).index(tblEl.get(0)) - 1;
						var tmp = new Date(options.current);
						var changed = false;
						var fillIt = false;
						if (parentEl.is('th')) {
							if (parentEl.hasClass('datepickerWeek') && options.mode == 'range' && !parentEl.next().hasClass('datepickerDisabled')) {
								var val = parseInt(parentEl.next().text(), 10);
								tmp.addMonths(tblIndex - Math.floor(options.calendars/2));
								if (parentEl.next().hasClass('datepickerNotInMonth')) {
									tmp.addMonths(val > 15 ? -1 : 1);
								}
								tmp.setDate(val);
								options.date[0] = (tmp.setHours(0,0,0,0)).valueOf();
								tmp.setHours(23,59,59,0);
								tmp.addDays(6);
								options.date[1] = tmp.valueOf();
								fillIt = true;
								changed = true;
								options.lastSel = false;
							} else if (parentEl.hasClass('datepickerMonth')) {
								tmp.addMonths(tblIndex - Math.floor(options.calendars/2));
								switch (tblEl.get(0).className) {
									case 'datepickerViewDays':
										tblEl.get(0).className = 'datepickerViewMonths';
										el.find('span').text(tmp.getFullYear());
										break;
									case 'datepickerViewMonths':
										tblEl.get(0).className = 'datepickerViewYears';
										el.find('span').text((tmp.getFullYear()-6) + ' - ' + (tmp.getFullYear()+5));
										break;
									case 'datepickerViewYears':
										tblEl.get(0).className = 'datepickerViewDays';
										el.find('span').text(formatDate(tmp, 'B, Y'));
										break;
								}
							} else if (parentEl.parent().parent().is('thead')) {
								switch (tblEl.get(0).className) {
									case 'datepickerViewDays':
										options.current.addMonths(parentEl.hasClass('datepickerGoPrev') ? -1 : 1);
										break;
									case 'datepickerViewMonths':
										options.current.addYears(parentEl.hasClass('datepickerGoPrev') ? -1 : 1);
										break;
									case 'datepickerViewYears':
										options.current.addYears(parentEl.hasClass('datepickerGoPrev') ? -12 : 12);
										break;
								}
								fillIt = true;
							}
						} else if (parentEl.is('td') && !parentEl.hasClass('datepickerDisabled')) {
							switch (tblEl.get(0).className) {
								case 'datepickerViewMonths':
									options.current.setMonth(tblEl.find('tbody.datepickerMonths td').index(parentEl));
									options.current.setFullYear(parseInt(tblEl.find('thead th.datepickerMonth span').text(), 10));
									options.current.addMonths(Math.floor(options.calendars/2) - tblIndex);
									tblEl.get(0).className = 'datepickerViewDays';
									changed = (options.view === 'months')
									break;
								case 'datepickerViewYears':
									options.current.setFullYear(parseInt(el.text(), 10));
									tblEl.get(0).className = 'datepickerViewMonths';
									changed = (options.view === 'years')
									break;
								default:
									var val = parseInt(el.text(), 10);
									tmp.addMonths(tblIndex - Math.floor(options.calendars/2));
									if (parentEl.hasClass('datepickerNotInMonth')) {
										tmp.addMonths(val > 15 ? -1 : 1);
									}
									tmp.setDate(val);
									switch (options.mode) {
										case 'multiple':
											val = (tmp.setHours(0,0,0,0)).valueOf();
											if ($.inArray(val, options.date) > -1) {
												$.each(options.date, function(nr, dat){
													if (dat == val) {
														options.date.splice(nr,1);
														return false;
													}
												});
											} else {
												options.date.push(val);
											}
											break;
										case 'range':
											if (!options.lastSel) {
												options.date[0] = (tmp.setHours(0,0,0,0)).valueOf();
											}
											val = (tmp.setHours(23,59,59,0)).valueOf();
											if (val < options.date[0]) {
												options.date[1] = options.date[0] + 86399000;
												options.date[0] = val - 86399000;
											} else {
												options.date[1] = val;
											}
											options.lastSel = !options.lastSel;
											break;
										default:
											options.date = tmp.valueOf();
											break;
									}
									
									
									changed = (options.view === 'days')
									break;
							}
							fillIt = true;
							//changed = true;
						}
						if (fillIt) {
							fill(this);
						}
						if (changed) {
							options.onChange.apply(this, prepareDate(options));
						}
					}
					return false;
				},
				prepareDate = function (options) {
					var tmp;
					if (options.mode == 'single') {
						tmp = new Date(options.date);
						return [formatDate(tmp, options.format), tmp, options.el];
					} else {
						tmp = [[],[], options.el];
						$.each(options.date, function(nr, val){
							var date = new Date(val);
							tmp[0].push(formatDate(date, options.format));
							tmp[1].push(date);
						});
						return tmp;
					}
				},
				getViewport = function () {
					var m = document.compatMode == 'CSS1Compat';
					return {
						l : window.pageXOffset || (m ? document.documentElement.scrollLeft : document.body.scrollLeft),
						t : window.pageYOffset || (m ? document.documentElement.scrollTop : document.body.scrollTop),
						w : window.innerWidth || (m ? document.documentElement.clientWidth : document.body.clientWidth),
						h : window.innerHeight || (m ? document.documentElement.clientHeight : document.body.clientHeight)
					};
				},
				isChildOf = function(parentEl, el, container) {
					if (parentEl == el) {
						return true;
					}
					if (parentEl.contains) {
						return parentEl.contains(el);
					}
					if ( parentEl.compareDocumentPosition ) {
						return !!(parentEl.compareDocumentPosition(el) & 16);
					}
					var prEl = el.parentNode;
					while(prEl && prEl != container) {
						if (prEl == parentEl)
							return true;
						prEl = prEl.parentNode;
					}
					return false;
				},
				show = function (ev) {
					var cal = $('#' + $(this).data('datepickerId'));
					if (!cal.is(':visible')) {
						var calEl = cal.get(0);
						fill(calEl);
						var options = cal.data('datepicker');
						options.onBeforeShow.apply(this, [cal.get(0)]);
						var pos = $(this).offset();
						var viewPort = getViewport();
						var top = pos.top;
						var left = pos.left;
						var oldDisplay = $(calEl).css('display');
						cal.css({
							visibility: 'hidden',
							display: 'block'
						});
						layout(calEl);
						switch (options.position){
							case 'top':
								top -= calEl.offsetHeight;
								break;
							case 'left':
								left -= calEl.offsetWidth;
								break;
							case 'right':
								left += this.offsetWidth;
								break;
							case 'bottom':
								top += this.offsetHeight;
								break;
						}
						if (top + calEl.offsetHeight > viewPort.t + viewPort.h) {
							top = pos.top  - calEl.offsetHeight;
						}
						if (top < viewPort.t) {
							top = pos.top + this.offsetHeight + calEl.offsetHeight;
						}
						if (left + calEl.offsetWidth > viewPort.l + viewPort.w) {
							left = pos.left - calEl.offsetWidth;
						}
						if (left < viewPort.l) {
							left = pos.left + this.offsetWidth
						}
						cal.css({
							visibility: 'visible',
							display: 'block',
							top: top + 'px',
							left: left + 'px'
						});
						if (options.onShow.apply(this, [cal.get(0)]) != false) {
							cal.show();
						}
						$(document).bind('mousedown', {cal: cal, trigger: this}, hide);
					}
					return false;
				},
				hide = function (ev) {
					if (ev.target != ev.data.trigger && !isChildOf(ev.data.cal.get(0), ev.target, ev.data.cal.get(0))) {
						if (ev.data.cal.data('datepicker').onHide.apply(this, [ev.data.cal.get(0)]) != false) {
							ev.data.cal.hide();
						}
						$(document).unbind('mousedown', hide);
					}
				};
			return {
				init: function(options){
					options = $.extend({}, defaults, options||{});
					extendDate(options.locale);
					options.calendars = Math.max(1, parseInt(options.calendars,10)||1);
					options.mode = /single|multiple|range/.test(options.mode) ? options.mode : 'single';
					return this.each(function(){
						if (!$(this).data('datepicker')) {
							options.el = this;
							if (options.date.constructor == String) {
								options.date = parseDate(options.date, options.format);
								options.date.setHours(0,0,0,0);
							}
							if (options.mode != 'single') {
								if (options.date.constructor != Array) {
									options.date = [options.date.valueOf()];
									if (options.mode == 'range') {
										options.date.push(((new Date(options.date[0])).setHours(23,59,59,0)).valueOf());
									}
								} else {
									for (var i = 0; i < options.date.length; i++) {
										options.date[i] = (parseDate(options.date[i], options.format).setHours(0,0,0,0)).valueOf();
									}
									if (options.mode == 'range') {
										options.date[1] = ((new Date(options.date[1])).setHours(23,59,59,0)).valueOf();
									}
								}
							} else {
								options.date = options.date.valueOf();
							}
							if (!options.current) {
								options.current = new Date();
							} else {
								options.current = parseDate(options.current, options.format);
							} 
							options.current.setDate(1);
							options.current.setHours(0,0,0,0);
							var id = 'datepicker_' + parseInt(Math.random() * 1000), cnt;
							options.id = id;
							$(this).data('datepickerId', options.id);
							var cal = $(tpl.wrapper).attr('id', id).bind('click', click).data('datepicker', options);
							if (options.className) {
								cal.addClass(options.className);
							}
							var html = '';
							for (var i = 0; i < options.calendars; i++) {
								cnt = options.starts;
								if (i > 0) {
									html += tpl.space;
								}
								html += tmpl(tpl.head.join(''), {
										week: options.locale.weekMin,
										prev: options.prev,
										next: options.next,
										day1: options.locale.daysMin[(cnt++)%7],
										day2: options.locale.daysMin[(cnt++)%7],
										day3: options.locale.daysMin[(cnt++)%7],
										day4: options.locale.daysMin[(cnt++)%7],
										day5: options.locale.daysMin[(cnt++)%7],
										day6: options.locale.daysMin[(cnt++)%7],
										day7: options.locale.daysMin[(cnt++)%7]
									});
							}
							cal
								.find('tr:first').append(html)
									.find('table').addClass(views[options.view]);
							fill(cal.get(0));
							if (options.flat) {
								cal.appendTo(this).show().css('position', 'relative');
								layout(cal.get(0));
							} else {
								cal.appendTo(document.body);
								$(this).bind(options.eventName, show);
							}
						}
					});
				},
				showPicker: function() {
					return this.each( function () {
						if ($(this).data('datepickerId')) {
							show.apply(this);
						}
					});
				},
				hidePicker: function() {
					return this.each( function () {
						if ($(this).data('datepickerId')) {
							$('#' + $(this).data('datepickerId')).hide();
						}
					});
				},
				setDate: function(date, shiftTo){
					return this.each(function(){
						if ($(this).data('datepickerId')) {
							var cal = $('#' + $(this).data('datepickerId'));
							var options = cal.data('datepicker');
							options.date = date;
							if (options.date.constructor == String) {
								options.date = parseDate(options.date, options.format);
								options.date.setHours(0,0,0,0);
							}
							if (options.mode != 'single') {
								if (options.date.constructor != Array) {
									options.date = [options.date.valueOf()];
									if (options.mode == 'range') {
										options.date.push(((new Date(options.date[0])).setHours(23,59,59,0)).valueOf());
									}
								} else {
									for (var i = 0; i < options.date.length; i++) {
										options.date[i] = (parseDate(options.date[i], options.format).setHours(0,0,0,0)).valueOf();
									}
									if (options.mode == 'range') {
										options.date[1] = ((new Date(options.date[1])).setHours(23,59,59,0)).valueOf();
									}
								}
							} else {
								options.date = options.date.valueOf();
							}
							if (shiftTo) {
								options.current = new Date (options.mode != 'single' ? options.date[0] : options.date);
							}
							fill(cal.get(0));
						}
					});
				},
				getDate: function(formated) {
					if (this.size() > 0) {
						return prepareDate($('#' + $(this).data('datepickerId')).data('datepicker'))[formated ? 0 : 1];
					}
				},
				clear: function(){
					return this.each(function(){
						if ($(this).data('datepickerId')) {
							var cal = $('#' + $(this).data('datepickerId'));
							var options = cal.data('datepicker');
							if (options.mode != 'single') {
								options.date = [];
								fill(cal.get(0));
							}
						}
					});
				},
				fixLayout: function(){
					return this.each(function(){
						if ($(this).data('datepickerId')) {
							var cal = $('#' + $(this).data('datepickerId'));
							var options = cal.data('datepicker');
							if (options.flat) {
								layout(cal.get(0));
							}
						}
					});
				}
			};
		}();
		$.fn.extend({
			DatePicker: DatePicker.init,
			DatePickerHide: DatePicker.hidePicker,
			DatePickerShow: DatePicker.showPicker,
			DatePickerSetDate: DatePicker.setDate,
			DatePickerGetDate: DatePicker.getDate,
			DatePickerClear: DatePicker.clear,
			DatePickerLayout: DatePicker.fixLayout
		});
	})(jQuery);

	(function(){
	  var cache = {};
	 
	  this.tmpl = function tmpl(str, data){
	    // Figure out if we're getting a template, or if we need to
	    // load the template - and be sure to cache the result.
	    var fn = !/\W/.test(str) ?
	      cache[str] = cache[str] ||
	        tmpl(document.getElementById(str).innerHTML) :
	     
	      // Generate a reusable function that will serve as a template
	      // generator (and which will be cached).
	      new Function("obj",
	        "var p=[],print=function(){p.push.apply(p,arguments);};" +
	       
	        // Introduce the data as local variables using with(){}
	        "with(obj){p.push('" +
	       
	        // Convert the template into pure JavaScript
	        str
	          .replace(/[\r\t\n]/g, " ")
	          .split("<%").join("\t")
	          .replace(/((^|%>)[^\t]*)'/g, "$1\r")
	          .replace(/\t=(.*?)%>/g, "',$1,'")
	          .split("\t").join("');")
	          .split("%>").join("p.push('")
	          .split("\r").join("\\'")
	      + "');}return p.join('');");
	   
	    // Provide some basic currying to the user
	    return data ? fn( data ) : fn;
	  };
	})();

}($,window[ns]));

(function($,context) {

	var els = $('span.input.date,span.date-input');

	//date inputs
	els.each(function() {
	
		var 
			scriptDfd,
			cssDfd = new $.Deferred(),
			buildDatePickers = function() {
			
				var 
					now  	= (new Date()).setHours(0,0,0,0),
					current	= (new Date()),
					num_calenders = 1;
					current.setMonth(new Date(now).getMonth());
					
				DPOptions = {
					eventName		:	'focus',
					date					:	current,
					current				:	current,
					calendars			:	num_calenders,
					starts				:	0,
					format				:	'b d, Y',
					view					:	'days',
					extraHeight		:	true,
					onBeforeShow	:	function() {
						var 
							el = $(this),
							d = el.data();
					},
					onRender : function(date) {
						var 
							linkedEl,
							linkedElDate,
							el = $(this.el),
							thisDate = el.data('hasdate') ? el.DatePickerGetDate() : null,
							disabled = ( date.valueOf() < now.valueOf() ),
							theClass = '';
						
						theClass += date.valueOf() === now.valueOf() ? ' today' : '';
						theClass += (thisDate && date.valueOf() == thisDate.valueOf()) ? ' actualDatepickerSelected' : '';
						
						if(el.data('linked')) {
							linkedEl = $(el.data('linked'));
							
							if(linkedEl.data('hasdate')) {
								linkedElDate = linkedEl.DatePickerGetDate();
								if(linkedElDate instanceof Date) {
								
									if(date.valueOf() === linkedElDate.valueOf()) {
										theClass += ' linkedDate';
									}
								
									if(linkedEl.data('start')) {
										if(date.valueOf() <= linkedElDate.valueOf()) {
											disabled = true;
										}
										
										if(thisDate) {
											if(date.valueOf() < thisDate.valueOf() && date.valueOf() >= linkedElDate.valueOf()) {
												theClass += ' linkedRange';
											}
										}
										
										
									} else if(linkedEl.data('finish')) {
									
										if(thisDate) {
											if(date.valueOf() > thisDate.valueOf() && date.valueOf() <= linkedElDate.valueOf()) {
												theClass += ' linkedRange';
											}
										}
									
										if(date.valueOf() >= linkedElDate.valueOf()) {
											//disabled = true;
										}
									
									}
								
								}
							}
							
						}
						
						return { 
							disabled : disabled, 
							className : theClass
						};
					},
					onChange : function(format,dateObj,input) {
						var $input = $(input);
						$input.data('hasdate',true);
						$input.DatePickerSetDate(dateObj);
						$input.val(format).DatePickerHide();
						
						if($input.data('linked')) {
							var linkedEl = $($input.data('linked'));
							
							if($input.data('start')) {
								//get the datepicker of the linkedEl and set current
								var 
									shouldUpdate = false,
									newEndDate = new Date( (dateObj.valueOf() + (60*60*24*1000) ) );
									
									if(linkedEl.data('hasdate')) {
										if(dateObj.valueOf() >= linkedEl.DatePickerGetDate().valueOf() ) {
											shouldUpdate = true;
										}
									} else {
										shouldUpdate = true;
									}
									
									if(shouldUpdate) {
										linkedEl
											.val('')
											.removeData('hasdate')
											.DatePickerSetDate(newEndDate,true)
											.focus();									
									}
							}

						}
						
					}
				};
			
				els.each(function() {
				
					if(Modernizr.touch) {
						
						var
							originalInput = $(this).find('input'),
							newInput = originalInput.clone();
						
						newInput.attr('type','date');
						newInput
							.on('focus',function() {
								$(this).parent().addClass('hasdate');
							})
							.on('change blur',function() {
								if(!$.trim($(this).val()).length) {
									$(this).parent().removeClass('hasdate');
								}
							});
						originalInput.replaceWith(newInput);
						newInput.before('<span class="input-label">'+originalInput.attr('placeholder')+'</span>');
						return;
					}
					
					$(this)
						.children()
						.on('keydown',function(e) { if(e.keyCode == 9) { $(this).DatePickerHide(); } })
						.DatePicker(DPOptions)
						.on('focus',function(e) {
							var 
								el = $(this),
								data = el.data();

							if(data['staticdp']) {
								$('#'+data.datepickerId).addClass('appended').insertAfter(el.parent());
							}

						});

				});
			
			};
	
		if($.fn.DatePicker || Modernizr.touch) {
			buildDatePickers.apply();
			return false;
		}
		
		
		return false;
	});
	
	//no public api
	return {};
		
}(jQuery,window[ns]));

(function(context) {

		/*
		 * Swipe 2.0
		 *
		 * Brad Birdsall
		 * Copyright 2013, MIT License
		 *
		*/

		function Swipe(container, options) {

		  "use strict";

		  // utilities
		  var noop = function() {}; // simple no operation function
		  var offloadFn = function(fn) { setTimeout(fn || noop, 0) }; // offload a functions execution

		  // check browser capabilities
		  var browser = {
			addEventListener: !!window.addEventListener,
			touch: ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch,
			transitions: (function(temp) {
			  var props = ['transitionProperty', 'WebkitTransition', 'MozTransition', 'OTransition', 'msTransition'];
			  for ( var i in props ) if (temp.style[ props[i] ] !== undefined) return true;
			  return false;
			})(document.createElement('swipe'))
		  };

		  // quit if no root element
		  if (!container) return;
		  var element = container.children[0];
		  var slides, slidePos, width, length;
		  options = options || {};
		  var index = parseInt(options.startSlide, 10) || 0;
		  var speed = options.speed || 300;
		  options.continuous = options.continuous !== undefined ? options.continuous : true;

		  function setup() {

			// cache slides
			slides = element.children;
			length = slides.length;

			// set continuous to false if only one slide
			if (slides.length < 2) options.continuous = false;

			//special case if two slides
			if (browser.transitions && options.continuous && slides.length < 3) {
			  element.appendChild(slides[0].cloneNode(true));
			  element.appendChild(element.children[1].cloneNode(true));
			  slides = element.children;
			}

			// create an array to store current positions of each slide
			slidePos = new Array(slides.length);

			// determine width of each slide
			width = container.getBoundingClientRect().width || container.offsetWidth;

			element.style.width = (slides.length * width) + 'px';

			// stack elements
			var pos = slides.length;
			while(pos--) {

			  var slide = slides[pos];

			  slide.style.width = width + 'px';
			  slide.setAttribute('data-index', pos);

			  if (browser.transitions) {
				slide.style.left = (pos * -width) + 'px';
				move(pos, index > pos ? -width : (index < pos ? width : 0), 0);
			  }

			}

			// reposition elements before and after index
			if (options.continuous && browser.transitions) {
			  move(circle(index-1), -width, 0);
			  move(circle(index+1), width, 0);
			}

			if (!browser.transitions) element.style.left = (index * -width) + 'px';

			container.style.visibility = 'visible';

		  }

		  function prev() {

			if (options.continuous) slide(index-1);
			else if (index) slide(index-1);

		  }

		  function next() {

			if (options.continuous) slide(index+1);
			else if (index < slides.length - 1) slide(index+1);

		  }

		  function circle(index) {

			// a simple positive modulo using slides.length
			return (slides.length + (index % slides.length)) % slides.length;

		  }

		  function slide(to, slideSpeed) {

			// do nothing if already on requested slide
			if (index == to) return;

			if (browser.transitions) {

			  var direction = Math.abs(index-to) / (index-to); // 1: backward, -1: forward

			  // get the actual position of the slide
			  if (options.continuous) {
				var natural_direction = direction;
				direction = -slidePos[circle(to)] / width;

				// if going forward but to < index, use to = slides.length + to
				// if going backward but to > index, use to = -slides.length + to
				if (direction !== natural_direction) to =  -direction * slides.length + to;

			  }

			  var diff = Math.abs(index-to) - 1;

			  // move all the slides between index and to in the right direction
			  while (diff--) move( circle((to > index ? to : index) - diff - 1), width * direction, 0);

			  to = circle(to);

			  move(index, width * direction, slideSpeed || speed);
			  move(to, 0, slideSpeed || speed);

			  if (options.continuous) move(circle(to - direction), -(width * direction), 0); // we need to get the next in place

			} else {

			  to = circle(to);
			  animate(index * -width, to * -width, slideSpeed || speed);
			  //no fallback for a circular continuous if the browser does not accept transitions
			}

			index = to;
			offloadFn(options.callback && options.callback(index, slides[index]));
		  }

		  function move(index, dist, speed) {

			translate(index, dist, speed);
			slidePos[index] = dist;

		  }

		  function translate(index, dist, speed) {

			var slide = slides[index];
			var style = slide && slide.style;

			if (!style) return;

			style.webkitTransitionDuration =
			style.MozTransitionDuration =
			style.msTransitionDuration =
			style.OTransitionDuration =
			style.transitionDuration = speed + 'ms';

			style.webkitTransform = 'translate(' + dist + 'px,0)' + 'translateZ(0)';
			style.msTransform =
			style.MozTransform =
			style.OTransform = 'translateX(' + dist + 'px)';

		  }

		  function animate(from, to, speed) {

			// if not an animation, just reposition
			if (!speed) {

			  element.style.left = to + 'px';
			  return;

			}

			var start = +new Date;

			var timer = setInterval(function() {

			  var timeElap = +new Date - start;

			  if (timeElap > speed) {

				element.style.left = to + 'px';

				if (delay) begin();

				options.transitionEnd && options.transitionEnd.call(event, index, slides[index]);

				clearInterval(timer);
				return;

			  }

			  element.style.left = (( (to - from) * (Math.floor((timeElap / speed) * 100) / 100) ) + from) + 'px';

			}, 4);

		  }

		  // setup auto slideshow
		  var delay = options.auto || 0;
		  var interval;

		  function begin() {

			interval = setTimeout(next, delay);

		  }

		  function stop() {

			delay = 0;
			clearTimeout(interval);

		  }


		  // setup initial vars
		  var start = {};
		  var delta = {};
		  var isScrolling;

		  // setup event capturing
		  var events = {

			handleEvent: function(event) {

			  switch (event.type) {
				case 'touchstart': this.start(event); break;
				case 'touchmove': this.move(event); break;
				case 'touchend': offloadFn(this.end(event)); break;
				case 'webkitTransitionEnd':
				case 'msTransitionEnd':
				case 'oTransitionEnd':
				case 'otransitionend':
				case 'transitionend': offloadFn(this.transitionEnd(event)); break;
				case 'resize': offloadFn(setup); break;
			  }

			  if (options.stopPropagation) event.stopPropagation();

			},
			start: function(event) {

			  var touches = event.touches[0];

			  // measure start values
			  start = {

				// get initial touch coords
				x: touches.pageX,
				y: touches.pageY,

				// store time to determine touch duration
				time: +new Date

			  };

			  // used for testing first move event
			  isScrolling = undefined;

			  // reset delta and end measurements
			  delta = {};

			  // attach touchmove and touchend listeners
			  element.addEventListener('touchmove', this, false);
			  element.addEventListener('touchend', this, false);

			},
			move: function(event) {

			  // ensure swiping with one touch and not pinching
			  if ( event.touches.length > 1 || event.scale && event.scale !== 1) return

			  if (options.disableScroll) event.preventDefault();

			  var touches = event.touches[0];

			  // measure change in x and y
			  delta = {
				x: touches.pageX - start.x,
				y: touches.pageY - start.y
			  }

			  // determine if scrolling test has run - one time test
			  if ( typeof isScrolling == 'undefined') {
				isScrolling = !!( isScrolling || Math.abs(delta.x) < Math.abs(delta.y) );
			  }

			  // if user is not trying to scroll vertically
			  if (!isScrolling) {

				// prevent native scrolling
				event.preventDefault();

				// stop slideshow
				stop();

				// increase resistance if first or last slide
				if (options.continuous) { // we don't add resistance at the end

				  translate(circle(index-1), delta.x + slidePos[circle(index-1)], 0);
				  translate(index, delta.x + slidePos[index], 0);
				  translate(circle(index+1), delta.x + slidePos[circle(index+1)], 0);

				} else {

				  delta.x =
					delta.x /
					  ( (!index && delta.x > 0               // if first slide and sliding left
						|| index == slides.length - 1        // or if last slide and sliding right
						&& delta.x < 0                       // and if sliding at all
					  ) ?
					  ( Math.abs(delta.x) / width + 1 )      // determine resistance level
					  : 1 );                                 // no resistance if false

				  // translate 1:1
				  translate(index-1, delta.x + slidePos[index-1], 0);
				  translate(index, delta.x + slidePos[index], 0);
				  translate(index+1, delta.x + slidePos[index+1], 0);
				}

			  }

			},
			end: function(event) {

			  // measure duration
			  var duration = +new Date - start.time;

			  // determine if slide attempt triggers next/prev slide
			  var isValidSlide =
					Number(duration) < 250               // if slide duration is less than 250ms
					&& Math.abs(delta.x) > 20            // and if slide amt is greater than 20px
					|| Math.abs(delta.x) > width/2;      // or if slide amt is greater than half the width

			  // determine if slide attempt is past start and end
			  var isPastBounds =
					!index && delta.x > 0                            // if first slide and slide amt is greater than 0
					|| index == slides.length - 1 && delta.x < 0;    // or if last slide and slide amt is less than 0

			  if (options.continuous) isPastBounds = false;

			  // determine direction of swipe (true:right, false:left)
			  var direction = delta.x < 0;

			  // if not scrolling vertically
			  if (!isScrolling) {

				if (isValidSlide && !isPastBounds) {

				  if (direction) {

					if (options.continuous) { // we need to get the next in this direction in place

					  move(circle(index-1), -width, 0);
					  move(circle(index+2), width, 0);

					} else {
					  move(index-1, -width, 0);
					}

					move(index, slidePos[index]-width, speed);
					move(circle(index+1), slidePos[circle(index+1)]-width, speed);
					index = circle(index+1);

				  } else {
					if (options.continuous) { // we need to get the next in this direction in place

					  move(circle(index+1), width, 0);
					  move(circle(index-2), -width, 0);

					} else {
					  move(index+1, width, 0);
					}

					move(index, slidePos[index]+width, speed);
					move(circle(index-1), slidePos[circle(index-1)]+width, speed);
					index = circle(index-1);

				  }

				  options.callback && options.callback(index, slides[index]);

				} else {

				  if (options.continuous) {

					move(circle(index-1), -width, speed);
					move(index, 0, speed);
					move(circle(index+1), width, speed);

				  } else {

					move(index-1, -width, speed);
					move(index, 0, speed);
					move(index+1, width, speed);
				  }

				}

			  }

			  // kill touchmove and touchend event listeners until touchstart called again
			  element.removeEventListener('touchmove', events, false)
			  element.removeEventListener('touchend', events, false)

			},
			transitionEnd: function(event) {

			  if (parseInt(event.target.getAttribute('data-index'), 10) == index) {

				if (delay) begin();

				options.transitionEnd && options.transitionEnd.call(event, index, slides[index]);

			  }

			}

		  }

		  // trigger setup
		  setup();

		  // start auto slideshow if applicable
		  if (delay) begin();


		  // add event listeners
		  if (browser.addEventListener) {

			// set touchstart event on element
			if (browser.touch) element.addEventListener('touchstart', events, false);

			if (browser.transitions) {
			  element.addEventListener('webkitTransitionEnd', events, false);
			  element.addEventListener('msTransitionEnd', events, false);
			  element.addEventListener('oTransitionEnd', events, false);
			  element.addEventListener('otransitionend', events, false);
			  element.addEventListener('transitionend', events, false);
			}

			// set resize event on window
			window.addEventListener('resize', events, false);

		  } else {

			window.onresize = function () { setup() }; // to play nice with old IE

		  }

		  // expose the Swipe API
		  return {
			setup: function() {

			  setup();

			},
			slide: function(to, speed) {

			  // cancel slideshow
			  stop();

			  slide(to, speed);

			},
			prev: function() {

			  // cancel slideshow
			  stop();

			  prev();

			},
			next: function() {

			  // cancel slideshow
			  stop();

			  next();

			},
			stop: function() {

			  // cancel slideshow
			  stop();

			},
			getPos: function() {

			  // return current index position
			  return index;

			},
			getNumSlides: function() {

			  // return total number of slides
			  return length;
			},
			kill: function() {

			  // cancel slideshow
			  stop();

			  // reset element
			  element.style.width = '';
			  element.style.left = '';

			  // reset slides
			  var pos = slides.length;
			  while(pos--) {

				var slide = slides[pos];
				slide.style.width = '';
				slide.style.left = '';

				if (browser.transitions) translate(pos, 0, 0);

			  }

			  // removed event listeners
			  if (browser.addEventListener) {

				// remove current event listeners
				element.removeEventListener('touchstart', events, false);
				element.removeEventListener('webkitTransitionEnd', events, false);
				element.removeEventListener('msTransitionEnd', events, false);
				element.removeEventListener('oTransitionEnd', events, false);
				element.removeEventListener('otransitionend', events, false);
				element.removeEventListener('transitionend', events, false);
				window.removeEventListener('resize', events, false);

			  }
			  else {

				window.onresize = null;

			  }

			}
		  }

		}


		if ( window.jQuery || window.Zepto ) {
		  (function($) {
			$.fn.Swipe = function(params) {
			  return this.each(function() {
				$(this).data('Swipe', new Swipe($(this)[0], params));
			  });
			}
		  })( window.jQuery || window.Zepto )
		}
		

	$.extend(context,{
		Swipe: Swipe
	});

}(window[ns]));


/* SimpleResponsiveFader */
(function($,context) {

	$.fn.SimpleResponsiveFader = function(opts) {			
		
		var SRFInterface = {
			allElements 	: this,	//store the jQuery object passed to the plugin in allElements for easy retrieval
			moveTo 		: function(i) { this.allElements.each(function() { var SRF = $(this).data('SimpleResponsiveFader'); if(SRF) { SRF.moveTo(i); } }); return this; },
			pause			: function() { this.allElements.each(function() { var SRF = $(this).data('SimpleResponsiveFader'); if(SRF) { SRF.pause(); } }); return this; },
			start				: function(_speed) { this.allElements.each(function() { var SRF = $(this).data('SimpleResponsiveFader'); if(SRF) { SRF.start(_speed); } }); return this; },
			
			//get the raw JavaScript SRF object
			raw				: function() { return this.allElements.data('SimpleResponsiveFader'); },
			
			
			//return to jQuery chainability
			end				: function() { return this.allElements.each(function() { }); }
		}
		
		this.each(function() { if(!$(this).data('SimpleResponsiveFader')) { $(this).data('SimpleResponsiveFader',new SimpleResponsiveFader($(this),opts)); } });	

		return SRFInterface;
		
	}
	
	
	function SimpleResponsiveFader(el,opts) {
		var o = this;
		
		this.state = {
			holder			:	el,
			list				:	el.children('.slider'),
			items			:	el.children('.slider').children(),
			total				:	el.children('.slider').children().length,
			interval			:	false,
			current			:	0,
			controls		:	false
		}
		
		if(!this.state.items.length) { return; }
		
		this.settings = $.extend({},$.fn.SimpleResponsiveFader.defaults,opts);
		this.state.items.eq(0).siblings().css('opacity',0);
		this.addListeners();
		this.state.holder.trigger('SRFInit',[this]);
		this.start();		
		
	}
	
	SimpleResponsiveFader.prototype.addListeners = function() {
		var 
			self 			=	this,
			holder		=	$('<div class="control-div"/>');
			controls 	= 	$('<ul class="slider_controls"/>'),
			links 		= 	$('<ul class="slider_links"/>'),
			linkCb		=	function(e) { self.moveTo($(this).closest('li').index()); $(this).closest('li').addClass('selected').siblings().removeClass('selected'); },
			controlCb	=	function(e) {
				var 
					el 		= 	$(this),
					which	= 	(el.hasClass('next') ? 1 : -1),
					next		=	(+self.state.current)+which,
					actual	=	(next >= self.state.items.length) ? 0 : next;
					
				self.moveTo(actual);
			};
			
		if(this.settings.pauseOnHover) {

			this.state.holder
				.on('mouseenter',function() { self.pause(); })
				.on('mouseleave',function() { self.start(); });
		}
			
		if(this.settings.buildControls && this.state.total > 1) {
			controls
				.append('<li><a class="next">&raquo;</a></li>')
				.append('<li><a class="prev">&laquo;</a></li>');
			
			holder.append(controls);
			this.state.controls = controls.find('a').on('click',controlCb);
		}
		
		if(this.settings.separateControls && this.settings.separateControls.length) { 
			this.settings.separateControls.on('click',controlCb);
		}
		
		if(this.settings.buildLinks && this.state.total > 1) {
			this.state.items.each(function(index,el) { links.append('<li><a>'+((+index)+1)+'</a></li>'); });
			holder.append(links);
			this.state.links = links.find('a').on('click',linkCb);
		}
		
		this.state.total > 1 && this.state.holder.prepend(holder);
		
		if(this.settings.separateLinks && this.settings.separateLinks.length) { this.settings.separateLinks.on('click',linkCb); }
		if(this.settings.centerLinks && this.state.links && this.state.links.length) { links.css('marginLeft',-(this.state.links.closest('ul').outerWidth()/2)).css('left','50%'); }
		
		//add the highlight link class to the selected link
		if(this.settings.highlightLink && this.state.links && this.state.links.length) {		
			links.find('a').eq(this.state.current).parent().addClass(this.settings.highlightLink);
		}
		
	
	}
	
	SimpleResponsiveFader.prototype.pause		=	function() {
		clearInterval(this.state.interval);
		this.state.paused = true;
		this.state.holder.trigger('SRFPause');
	}
	
	SimpleResponsiveFader.prototype.start		=	function(_speed,notrigger) {
		var 
				self 		= 	this,
				next		=	self.state.current-0+1,
				actual	=	(next >= this.state.items.length) ? 0 : next;
		
		this.state.paused = false;
		this.settings.speed = _speed || this.settings.speed;
		this.state.interval = setTimeout(function() { 
			self.moveTo(actual); 
		},self.settings.delay);
		
		if(!notrigger) { this.state.holder.trigger('SRFPause'); }
	}
	
	SimpleResponsiveFader.prototype.moveTo = function(i) {
		var 
			self		=	this,
			next 		= 	this.state.items.eq(i),
			current 	=	this.state.items.eq(this.state.current),
			finish	=	function() {
				next
					.siblings()
					.css('position','absolute')
					.css('opacity',0)
					.css('zIndex',-1)
					.end()
					.css('position','relative')
					.css('zIndex',1);
				
				self.state.animating = false;
				self.state.holder.trigger('SRFFadeEnd',[self]);
				self.state.paused || self.start(false,true);
			};
		
		if(this.state.animating || !next.length || this.state.current === i) { return; }
		clearInterval(this.state.interval);
		
		this.state.current = next.index();
		this.state.holder.trigger('SRFFadeStart',[this]);
		if(this.settings.highlightLink && this.state.links && this.state.links.length) {		
			this.state.links.eq(this.state.current).parent().addClass(this.settings.highlightLink).siblings().removeClass(this.settings.highlightLink);
		}
		this.state.animating = true;
		next.css('display','block').css('position','absolute').css('opacity',0).css('zIndex',10);
		
		if(this.settings.resizeHeight) { this.state.holder.stop().animate({height:next.outerHeight()},this.settings.resizeHeightSpeed,this.settings.resizeHeightEasing); }
		
		switch(this.settings.fadeType) {
			case 'in-out':
				current.stop().animate({opacity:0},this.settings.speed,this.settings.easing,function() {				
					next.stop().animate({opacity:1},self.settings.speed,self.settings.easing,finish);
				});			
				break;
			default:
				next.stop().animate({opacity:1},this.settings.speed,this.settings.easing,finish,function() {
					current.stop().animate({opacity:0},self.settings.speed,self.settings.easing);
				});
				break;
		}
		
	}
	
	
	$.fn.SimpleResponsiveFader.defaults = {
		delay					:	3000,
		easing					:	'swing',
		speed					:	1000,
		pauseOnHover		:	false,
		fadeType				:	'cross',			//string	- cross | in-out				-  	the fader will cross fade, or fade completely out and then in
		
		//animate the height when switching slides?
		resizeHeight				:	false,
		resizeHeightSpeed		:	1000,
		resizeHeightEasing	:	'swing',
		
		//next/previous controls
		buildControls			:	false,
		separateControls	:	false,
		
		
		//1, 2, 3, 4, ... links
		buildLinks				:	false,
		separateLinks		:	false,
		centerLinks			:	true,
		highlightLink			:	'selected'		//class name to be applied to the selected link
	}

	$.extend(context,{
		SimpleResponsiveFader: SimpleResponsiveFader
	});
	
}(jQuery,window[ns]));


(function(context) {

	var 
		Swipe = context.Swipe,
		SimpleResponsiveFader = context.SimpleResponsiveFader,
		buildSwipe,
		buildSRF;
		
	//SRF
	(function() {
		buildSRF = function() {
			//if a touch enabled device, use a swiper instead
			if(Modernizr.touch && !$(this).hasClass('force-srf')) {
				$(this)
					.removeClass('srf')
					.addClass('swipe')
					.children()
					.removeClass('slider')
					.addClass('swipe-wrap')
				return;
			}
		
				var 
					el = $(this);
					
				if($('html').hasClass('ie8') && el.data('noie8')) { return; }
					
				el
					.children()
					.addClass('slider')
					.end()
					.SimpleResponsiveFader({
						delay			:	( (+el.data('auto')*1000) || 5000),
						speed			:	500,
						resizeHeight	:	false,
						resizeHeightSpeed: 500,
						fadeType		:	el.data('type') || 'cross',
						pauseOnHover : !!(el.data('pauseonhover')),
						buildLinks		: 	!!(el.data('controls')),
						centerLinks	: 	false,
						buildControls	:	!!(el.data('links'))
					});
					
					if(!el.data('auto')) { el.SimpleResponsiveFader().pause(); }
		};
		
		$('div.srf').each(function() { buildSRF.apply(this,arguments); });
	}());
	
	//swiper
	(function() {
		buildSwipe = function() {
			var 
				swipe,
				el = $(this),
				//links = $(this).find('div.links a'),
				children = el.children('div.swipe-wrap').children('div');
				
				if(el.data('swipe')) { return; }
				
				if(children.length == 1) {
					el.css('visibility','visible');
				} else {
				
					swipe = new Swipe(el[0],{
						continuous: (el.data('notcontinuous') !== undefined) ? el.data('notcontinuous') : true,
						auto:( (+el.data('auto')*1000) || false),
						startSlide: (el.data('start')) || 0,
						stopOnClick:  (el.data('stoponclick') !== undefined) ? el.data('stoponclick') : true,
						speed: (+el.data('speed')) || 500,
						callback: function(pos,div) {
							var 
								i,
								d = $(div).data();
								
								if(d) {
									i = (typeof d.originalindex === 'undefined') ? d.index : d.originalindex;
								} else {
									i = pos;
									//return;
								}
							
							$('div.swipe',el).each(function() { buildSwipe.apply(this,arguments); });
						
							el.trigger('swipeChanged',[i,div]);
							if(el.data('controls') || el.data('controlbar')) {	
								el.parent().find('div.slider-controls a,div.slider-controls-links a.item').eq(i).addClass('selected').siblings().removeClass('selected');
							}
							
						}
					});
				
				}
				
				if(el.closest('div.swiper-wrapper').hasClass('paginated-swiper')) {
					(function() {
					
						if(children.length <= 1) {
							el.closest('div.swiper-wrapper').addClass('hide-links');
							return;
						}
					
						var
							pel = el.closest('div.swiper-wrapper'),
							paginationEl = $('div.pagination',pel),
							total = $('span.total',paginationEl),
							current = $('span.current',paginationEl),
							swiperEl = pel.children('div.swipe'),
							swiper = swiperEl.data('swipe'),
							perSlide = $('div.grid',pel).eq(0).children().length,
							totalCount = parseInt(total.html(),10);
							
							paginationEl.removeClass('hide');
							
							swiperEl.on('swipeChanged',function(e,i,div) {
								current.html('Page '+(i+1)+' of '+children.length);
							});
							
							
					}());
				}
				
				el.children('div.swipe-wrap').children('div').each(function(i,el) {
					if(i > children.length-1) {
						$(this).data('originalindex',((i%children.length))).addClass('ghost');
					}
				});
				
				el.data('swipe',swipe);
				
			if(children.length > 1) {
			
				if(el.data('controlbar')) {
				
					var controlString = '<div class="slider-controls-links swiper-nav-bar pagination"><div><a class="previous sprite" title="Previous">Previous</a>';
					children.each(function(i,el) {
						controlString += '<a class="item '+((!i) ? 'selected' : '')+'">'+(i+1)+'</a>';
					});
					controlString += '<a class="next sprite" title="Next">Next</a></div></div>';
					el
						.parent()
						.append(controlString)
						.on('click','div.slider-controls-links a',function(e) {
							e.preventDefault();
							var el = $(this);
							
							e.preventDefault();
							
							if(el.hasClass('next')) {
								swipe.next();
								return;
							}
							
							if(el.hasClass('prev')) {
								swipe.prev();
								return;
							}
							
							swipe.slide(el.parent().children('a.item').index(el));
						});
						
					return;
				
				}

			
				if(el.data('controls')) {
					
					var controlString = '<div class="slider-controls"><div>';
					children.each(function(i,el) {
						controlString += '<a class="'+((!i) ? 'selected' : '')+'">'+(i+1)+'</a>';
					});
					controlString += '</div></div>';
					el
						.parent()
						.append(controlString);
					
					
				}
			
				if(el.data('links')) {
					el
						.parent()
						.append('<div class="links"><a class="previous sprite" title="Previous">Previous</a><a class="next sprite" title="Next">Next</a></div>');
				}
				
				//add the listeners
					el
						.parent()
						.on('click','div.slider-controls a',function(e) {
							e.preventDefault();
							swipe.slide($(this).index());
							
							//restart, if required
							
							
						})
						.on('click','div.links a',function(e) {
							
							e.preventDefault();
							var el = $(this);
							if(el.hasClass('next')) {
								swipe.next();
							} else {
								swipe.prev();
							}
						});
			}
		};

		$('div.swipe').each(function() { buildSwipe.apply(this,arguments); });
	}());


	$.extend(context,{
		SwipeSRF: {
			buildSwipe: buildSwipe,
			buildSRF: buildSRF					
		}
	});

}(window[ns]));

(function(context) {

	if(window.devicePixelRatio) {
	
		if(window.devicePixelRatio > 2 ) { //i.e. 2.5
			$('html').addClass('threex');
			
		} else if(window.devicePixelRatio > 1) { //i.e. 1.5
			$('html').addClass('twox');
		}
	}

}(window[ns]));

(function(context) {

	var 
		query = '',
		req,
		$html = $('html'),
		$overlay = $('div.search-overlay'),
		$results = $overlay.find('div.search-results'),
		$form = $overlay.find('form'),
		$input = $form.children('input'),
		url = $form.attr('action'), 
		showclass = 'show-search',
		loadingclass = 'is-loading',
		noresultsclass = 'no-results',
		withresultsclass = 'with-results',
		methods = {
			
			scrollResultsToTop: function() {
				$results.find('div.results').scrollTop(0);
			},
	
			finishedSearch: function(r,status,jqXHR) {
				if(status === 'success' && jqXHR && r) {
					$results.html(r);
					$results.find('select').trigger('change');
					$overlay.removeClass(noresultsclass).addClass(withresultsclass);
					return;
				}
				
				this.failedSearch(arguments);
				
			},
			
			failedSearch: function(jqXHR,status,error) {
				if(status !== 'abort') {
					alert('No results found for '+query);
				}
			},
			
			completedRequest: function() {
				this.hideLoading();
				$input.blur();
			},
			
			showLoading: function() {
				$overlay.addClass(loadingclass);
			},
			
			hideLoading: function() {
				$overlay.removeClass(loadingclass);
			},
		
			doSearch: function(q) {
				var self = this,dfd = $.Deferred();
				
				query = q;
				
				self.showLoading();
				
				req && req.abort();				

				//for effect
				setTimeout(function() {

					req = $.ajax({
						url:url+'?q='+q
					})
						.done(function() {
							dfd.resolve();
							self.finishedSearch.apply(self,arguments);
						})
						.fail(function() {
							dfd.reject();
							self.failedSearch.apply(self,arguments);
						})
						.always(function() {
							self.completedRequest.apply(self,arguments);
						});

				},500);

					
				return dfd.promise();
			
			},
		
			showForm: function() {
				this.isVisible() || $overlay.addClass(noresultsclass).removeClass(withresultsclass);
				$html.addClass(showclass);
				$input.val('');
				Modernizr.touch || $input.focus();

			},
			
			hideForm: function() {
				$html.removeClass(showclass);
				$input.blur();
			},
			
			isVisible: function() {
				return $html.hasClass(showclass);
			},
			
			toggleForm: function() {
				if(!this.isVisible()) {
					return this.showForm();
				}
				
				return this.hideForm();
			}
	
		};
	
	$(document)
		.on('click','.toggle-search-form',function(e) {
			e.preventDefault();
			methods.toggleForm();
		})
		.on('keydown',function(e) {
			e.keyCode === 27 && methods.hideForm();
		});
		
	$overlay
		.on('change','select.tab-controller',function(e) {
			methods.scrollResultsToTop();
		});
	
	$form.on('submit',function(e) {
		methods.doSearch($input.val());
		return false;
	});


	return methods;
}(window[ns]));

(function(context) {

	var
		$document = $(document),
		$body = $('body'),
		$reservations = $('div.reservations'),
		SHOW_CLASS = 'show-reservations',
		ANIMATING_CLASS = 'animating animating-reservations';

	//mobile reservations
	(function() {
	
		var cb = function(e) { 
			var el = e.target ? e.target : e.srcElement;
			
			if($(el).hasClass('page-wrapper')) {
				$body.removeClass(ANIMATING_CLASS);
			}
		}
		$document.on('click','#mobile-book',function(e) {
			methods.toggleForm();
			return false;
		});
		
		$body
			.on({
				'webkitTransitionEnd': cb,
				'msTransitionEnd': cb,
				'oTransitionEnd': cb,
				'otransitionend': cb,
				'transitionend': cb
			});
		
	}());		

	var methods = {
		showForm: function(show) {
			$body[show ? 'addClass' : 'removeClass'](SHOW_CLASS);

			if(Modernizr.csstransforms3d) {
				$body.addClass(ANIMATING_CLASS);
			}			
		},

		toggleForm: function() {
			this.showForm(!this.isShowingForm());
		},

		isShowingForm: function() {
			return $body.hasClass(SHOW_CLASS);
		}
	};

	$reservations
		.on('click','.add-room',function(e) {
			var 
				el = $(this),
				parent = el.parent(),
				roomType = parent.find('div.room-type'),
				inputName = roomType.find('input').attr('name'),
				select = roomType.find('select'),
				selectName = select.attr('name'),
				index = roomType.last().index();

				roomType
					.eq(0)
					.clone()
					.find('input')
					.attr('name',inputName+'-'+index)
					.end()
					.insertAfter(roomType.eq(0));

		}).on('change','input[type=radio]',function() {
		
			var 
				el = $(this),
				select = el.closest('div.room-type,div.rate-type').find('select');

				$body.find('input[type=radio][name='+this.name+']').parent().removeClass('checked');
				el.parent().addClass('checked');

				select.val(el.val()).trigger('change');
				
		}).on('change','select',function(e) {
		
			var
				el = $(this),
				radios = el.closest('div.room-type,div.rate-type').find('input[type=radio]');

				radios
					.parent()
					.removeClass('checked')
					.end()
					.removeProp('checked')
					.removeAttr('checked')
					.eq(this.selectedIndex)
					.attr('checked','checked')
					.prop('checked','checked')
					.parent()
					.addClass('checked');
		}).on('click','.toggle',function(e) {
			$reservations.removeClass('calendar-focused').toggleClass('expanded');
		}).on('focus','#check-in,#check-out',function(e) {
			$reservations.addClass('calendar-focused');
		});

}(window[ns]));

(function(context) {

	var SwipeSRF = context.SwipeSRF;


	$('div.hero').each(function() {
		var
			el = $(this),
			swiperEl = $('div.swipe',el),
			swiper = swiperEl.data('swipe'),
			allSlides = swiperEl.find('div.swipe-wrap').children(),
			realSlides = allSlides.filter(function() { return !$(this).hasClass('ghost'); }),
			fakeSlides = allSlides.filter(function() { return realSlides.index(this) === -1 });
			utils = {
			
				loadBackgroundImageForSlideAtIndex: function(i) {
					
					var 
						slide = realSlides.eq(i),
						source = slide.data('src');
						
					if(slide.hasClass('loaded')) { return; }
					
					$('<img/>')
						.on('load',function() {
							realSlides
								.eq(i)
								.add(fakeSlides.eq(i))
								.children('div.item')
								.css('backgroundImage','url('+source+')')
								.parent()
								.addClass('loaded');
							
							//first load
							if(i === 0) {
								el.addClass('loaded');
							}
							
						})
						.attr('src',source);
					
				}
			
			};
			
			swiperEl.on('swipeChanged',function(e,i) {
				utils.loadBackgroundImageForSlideAtIndex(i);
			});
			
			//load the first background image immediately
			utils.loadBackgroundImageForSlideAtIndex(0);
			
	});

}(window[ns]));

(function(context) {

	var 
		SwipeSRF = context.SwipeSRF,
		$document = $(document),
		$body = $('body'),
		$nav = $('nav'),
		SHOW_CLASS = 'show-nav',
		ANIMATING_CLASS = 'animating animating-nav';


	//mobile nav
	(function() {
	
		var cb = function(e) { 
			var el = e.target ? e.target : e.srcElement;
			
			if($(el).hasClass('page-wrapper')) {
				$body.removeClass(ANIMATING_CLASS);
			}
		}
		$document.on('click','#mobile-nav',function(e) {
			methods.toggleNav();
			return false;
		});
		
		$body
			.on({
				'webkitTransitionEnd': cb,
				'msTransitionEnd': cb,
				'oTransitionEnd': cb,
				'otransitionend': cb,
				'transitionend': cb
			});
		
	}());

	var methods = {

		showNav: function(show) {
			$body[show ? 'addClass' : 'removeClass'](SHOW_CLASS);

			if(Modernizr.csstransforms3d) {
				$body.addClass(ANIMATING_CLASS);
			}

		},

		toggleNav: function() {
			this.showNav(!this.isShowingNav());
		},

		isShowingNav: function() {
			return $body.hasClass(SHOW_CLASS);
		}

	};

	
	//sliders in nav dropdowns
	$nav
		.on('mouseenter','>ul>li',function(e) {
			var
				el  =$(this),
				dd = el.children('div'),
				swiperEl = dd.find('div.swipe'),
				swiper = swiperEl.data('swipe');
			
				//rebuild
				if(swiper) {
					swiper.setup();
					swiperEl.trigger('swipeChanged',[0,swiperEl.find('div.img').eq(0).parent()[0]]);
				}
				
		})
		.on('mouseenter','li li a',function(e) {
			var
				el = $(this),
				index = el.parent().index(),
				dd = el.closest('div'),
				swiper = dd.find('div.swipe');
				
				swiper.data('swipe').slide(index);
		})
		.on('swipeChanged',function(e,index,element) {
			var
				source,
				el = $(element),
				imgEl = el.find('div.img'),
				imgSrc = imgEl.data('img');
				
				if(imgEl.data('working')) { return; }
				
				imgEl.data('working',true);
				
				if(imgSrc) {
					source = templateJS.templateURL+'/'+imgSrc;
					$('<img/>')
						.on('load',function() {
							imgEl
								.css('backgroundImage','url('+source+')')
								.addClass('loaded')
								.removeData('working')
								.removeData('img');
						})
						.on('error',function() {
							imgEl.removeData('working');
						})
						.attr('src',source);
				}
				
		});
		

	//no public API
	return {};

}(window[ns]));

//ios - good to use on combination with touch
Modernizr.addTest('ios',function() {
	return window.navigator.userAgent.match(/iphone|ipad/i);
});

//android
Modernizr.addTest('android',function() {
	return window.navigator.userAgent.match(/android/i);
});

