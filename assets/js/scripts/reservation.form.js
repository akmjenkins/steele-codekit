(function(context) {

	var
		$document = $(document),
		$body = $('body'),
		$reservations = $('div.reservations'),
		SHOW_CLASS = 'show-reservations',
		ANIMATING_CLASS = 'animating animating-reservations';

	//mobile reservations
	(function() {
	
		var cb = function(e) { 
			var el = e.target ? e.target : e.srcElement;
			
			if($(el).hasClass('page-wrapper')) {
				$body.removeClass(ANIMATING_CLASS);
			}
		}
		$document.on('click','#mobile-book',function(e) {
			methods.toggleForm();
			return false;
		});
		
		$body
			.on({
				'webkitTransitionEnd': cb,
				'msTransitionEnd': cb,
				'oTransitionEnd': cb,
				'otransitionend': cb,
				'transitionend': cb
			});
		
	}());		

	var methods = {
		showForm: function(show) {
			$body[show ? 'addClass' : 'removeClass'](SHOW_CLASS);

			if(Modernizr.csstransforms3d) {
				$body.addClass(ANIMATING_CLASS);
			}			
		},

		toggleForm: function() {
			this.showForm(!this.isShowingForm());
		},

		isShowingForm: function() {
			return $body.hasClass(SHOW_CLASS);
		}
	};

	$reservations
		.on('click','.add-room',function(e) {
			var 
				el = $(this),
				parent = el.parent(),
				roomType = parent.find('div.room-type'),
				inputName = roomType.find('input').attr('name'),
				select = roomType.find('select'),
				selectName = select.attr('name'),
				index = roomType.last().index();

				roomType
					.eq(0)
					.clone()
					.find('input')
					.attr('name',inputName+'-'+index)
					.end()
					.insertAfter(roomType.eq(0));

		}).on('change','input[type=radio]',function() {
		
			var 
				el = $(this),
				select = el.closest('div.room-type,div.rate-type').find('select');

				$body.find('input[type=radio][name='+this.name+']').parent().removeClass('checked');
				el.parent().addClass('checked');

				select.val(el.val()).trigger('change');
				
		}).on('change','select',function(e) {
		
			var
				el = $(this),
				radios = el.closest('div.room-type,div.rate-type').find('input[type=radio]');

				radios
					.parent()
					.removeClass('checked')
					.end()
					.removeProp('checked')
					.removeAttr('checked')
					.eq(this.selectedIndex)
					.attr('checked','checked')
					.prop('checked','checked')
					.parent()
					.addClass('checked');
		}).on('click','.toggle',function(e) {
			$reservations.removeClass('calendar-focused').toggleClass('expanded');
		}).on('focus','#check-in,#check-out',function(e) {
			$reservations.addClass('calendar-focused');
		});

}(window[ns]));