//background clip
Modernizr.addTest('backgroundclip',function() {

	var div = document.createElement('div'), success = false;

	if ('backgroundClip' in div.style) {
		div.style.backgroundClip = 'text';
		if(div.style.backgroundClip === 'text') {
			return true;
		}
	}
	
	'Webkit Moz O ms Khtml'.replace(/([A-Za-z]+)/g,function(val) {
		  if (val+'BackgroundClip' in div.style) {
			div.style[val+'BackgroundClip'] = 'text';
			if(div.style[val+'BackgroundClip'] === 'text') {
				success = true;
				return true;
			}
		}
	});
	
	return success;
});